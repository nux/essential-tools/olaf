'''
All the functions listed here are executed once per shot, and they have access only to the data relative to the given shot
The function must have the following form:


FUNCTION DEFINITION
-------------------

def FUNCION_NAME(settings, experiment_data, properties)

- settings: a dictionary containing all the settings loaded from the configuration file (yaml format) and parsed as nested dictionaries
- experiment_data: contains the data loaded from the h5 file, following the scheme indicated in the "h5" section of the configuration file.
- properties: previously computed properties, to allow conditional execution of the function basing for the previous analysis results

For example, if the diffraction patterns are in the dataset "vmi/andor" in the h5 file, and the configuration file contains the following:

h5:
    pattern: "vmi/andor"

then, the pattern relative to the shot is stored in experiment_data["pattern"].
Moreover, it's previously computed property "markername" can be accessed via properties["markername"]

FUNCTION RETURN
---------------

1) The function MUST return a dictionary or nothing. Each key of a dictionary defines a marker name in the analysis file. The dictionary can be empty, or no return value is given, which means that no marker is added to the analysis (useful, for example, if the function is used for data preparation)
2) The returned dictionary can contain multiple keys. The value of each key must be a single value 
    
    
IMPORTANT NOTES
---------------

* The functions are called in the order they are written in this file. 

* All the functions are executed. However, it is possible to create local functions, that are not directly called during the analysis, by giving them a name that starts with "_". For example, the following function:

def _get_sum(a,b):
    return a+b
    
is ok, and can be used by the other functions in this file. Instead, the function

def get_sum(a,b):
    return a+b
    
will be executed during the analysis.

* the types of the markers must be native python types, which means int, float and string

'''


tof_windows={
    "H+":[5404,5544],
    "He+": [5850,6050],
    "H2O+": [6750,7216]    
    }

import numpy as np
import matplotlib.pyplot as plt
def get_id(settings, experiment_data, properties):
    '''
    Get the bunch id of the shot
    '''    
    return {"id": int(experiment_data["id"])}


def get_tof_intensity(settings, experiment_data, properties):
    '''
    Get the integral of the tof signal
    '''
    #if properties["hit"]==1:
    dataraw=experiment_data["tof"]
    threshold = -3
    data = np.abs(dataraw - np.mean(dataraw[1:4000]), dtype=float)
    
    #fig, ax=plt.subplots(1, len(tof_windows.keys())+1, figsize=(4*len(tof_windows.keys())+1, 4))
    
    tof_marks={}
    
    for index,key in enumerate(tof_windows.keys()):
        pardata=data[tof_windows[key][0]:tof_windows[key][1]]
        tof_marks["TOF_"+key]=np.sum(pardata)
        #ax[index].plot(pardata)
        #ax[index].set_title(key)
        
    #ax[-1].plot(data)
    #plt.show()
    #fig.close()
    
    #plt.plot(data)
    #plt.show()
    return tof_marks
    
    
