#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Miscellaneous functions and classes for beamtime 2857 at SQS / European XFEL.

:Author:    Bjoern Bastian <bbastian@phys.au.dk>
:Copyright: University of Aarhus, Department of Physics and Astronomy, 2022
"""

import functools
import os
import sys
from typing import Callable

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
#from IPython.display import display
from extra_geom import PNCCDGeometry
import extra_data as xd



class RunLoader():
    """
    Class to store a sources dictionary and provide a function to load
    data selected by run number, a selection and a source label.
    """
        

    def __init__(self, proposalNo, runNo, pattern_src_key, tof_src_key, cacheinfo, addkeys = [], chunksize=100):
        """
        Initialize object with an optional list of source, key tuples
        that are preselected to speed loading of metadata.
        """
        self.pattern_src_key = tuple(pattern_src_key)
        self.tof_src_key = None 
        if tof_src_key is not None:
            self.tof_src_key = tuple(tof_src_key)
        self.default_src_key = None

        
        self.maskparam={"maskAvg": 10000,
                        "maskStd": 5000,
                        "maskMinStd": 4,
                        "maskAvgToStd": 3000}

        
        self.proposalNo=proposalNo #2857
        self.runNo=runNo
        self.chunksize=chunksize
        
        self.data = xd.open_run(proposal=self.proposalNo, run=runNo)
        
        runstr = '{:04d}'.format(runNo)  
        self.bgcachefile= os.path.join(cacheinfo["folder"], cacheinfo["bgformat"]).replace("*", runstr)
        self.maskcachefile= os.path.join(cacheinfo["folder"], cacheinfo["maskformat"]).replace("*", runstr)
        
        if cacheinfo["staticmask"] =="":
            self.maskstaticfile=None
        
        else:
            self.maskstaticfile= os.path.join(cacheinfo["folder"], cacheinfo["staticmask"]).replace("*", runstr)
        
        self.pnccdpos_src_key = {
            'pnccdUp' : ('SQS_NQS_PNCCD/MOTOR/PNCCD_UP','actualPosition.value'),
            'pnccdDown' : ('SQS_NQS_PNCCD/MOTOR/PNCCD_DOWN','actualPosition.value'),
            'pnccdLeft' : ('SQS_NQS_PNCCD/MOTOR/PNCCD_RIGHT','actualPosition.value'),
            'pnccdRight' : ('SQS_NQS_PNCCD/MOTOR/PNCCD_LEFT','actualPosition.value'),
            'pnccdFEL' : ('SQS_NQS_PNCCD/MOTOR/PNCCD_FEL','actualPosition.value'),
            }
        
        

        self.pnccdoffsets={
            "gapAdd":1.3,
            "moveAdd":0.225,
            "closedPnccdUp" : 16.32,
            "closedPnccdDown" : 18.56,
            "rightStart" : 15.98414063,
            "leftStart" : 21.10546875,           
            }
        #self.pnccdpos_src_key = {
            #'pnccdUp' : ('SQS_NQS_PNCCD/MOTOR/PNCCD_UP','encoderPosition.value'),
            #'pnccdDown' : ('SQS_NQS_PNCCD/MOTOR/PNCCD_DOWN','encoderPosition.value'),
            #'pnccdLeft' : ('SQS_NQS_PNCCD/MOTOR/PNCCD_RIGHT','encoderPosition.value'),
            #'pnccdRight' : ('SQS_NQS_PNCCD/MOTOR/PNCCD_LEFT','encoderPosition.value'),
            #}        
        
        
        self.filter_src_keys = [self.pattern_src_key] + list(self.pnccdpos_src_key.values()) + addkeys
        if self.tof_src_key is not None:
            self.filter_src_keys.append(self.tof_src_key)
        self.data = self.data.select(self.filter_src_keys, require_all=True)
        self.datachunks = list(self.data.split_trains(trains_per_part=self.chunksize))
        
        self.getGeometry()

        if self.maskstaticfile is not None:
            print("Loading static mask file:", self.maskstaticfile)
            self.maskstatic = np.load(self.maskstaticfile)
        else:
            self.maskstatic = None
        
        if os.path.exists(self.maskcachefile) and cacheinfo['overwrite']==0:
            print("Loading existing mask file:", self.maskcachefile)
            self.mask = np.load(self.maskcachefile)
        else:
            print("Calculating mask for run", self.runNo)
            self.mask = self.getMask()
            print("Saving mask in", self.maskcachefile)
            np.save(self.maskcachefile, self.mask)
            
        if os.path.exists(self.bgcachefile) and cacheinfo['overwrite']==0:
            print("Loading existing background file:", self.bgcachefile)
            self.bg = np.load(self.bgcachefile)
        else:
            print("Calculating background for run", self.runNo)
            self.bg = self.getBackground(self.mask)
            print("Saving background in", self.bgcachefile)
            np.save(self.bgcachefile, self.bg)
        

    #def __call__(self, runNo, trainId=None, select=None, src_key=False):
        #"""
        #This function will return data from a run number runNo. The type of the data depends
        #on the optional keyword arguments.

        #If given a trainId, getData will return data of instrument src_key, matching this.
        #If the sources dictionary doesn't exist yet, it will return the full DataCollection
        #Else it will return keyData of instrument src_key. It can also be passed a select
        #to constrain the returned trains.
        #If ``src_key`` is ``False``, the default source set by setDefaultSource() is used.

        #- without keyword argument and ``src_key=None`` return ``DataCollection`` for the run
        #- without keyword argument and no default source return ``DataCollection`` for the run
        #- with ``src_key`` or default source return ``KeyData`` for the selected run and source
        #- with ``select=np.s_[:]`` or ``select=by_id[:]`` restrict the range of selected trains
        #- with ``trainId`` return a single data array
        #- ``getData(runNo, trainId)`` returns an array for the specified train ID
        #- ``getData(runNo, src_key=(source, key)).xarray()`` returns an array for all trains
        #- ``getData(runNo, select=0).xarray()`` returns an array with the first train
        #"""
        #data = xd.open_run(proposal=self.proposalNo, run=runNo)
        #if self.filter_src_keys is not None:
            #data = data.select(self.filter_src_keys)

        #if select is not None:
            #data  = data.select_trains(select)

        #if src_key is False:
            #src_key = self.default_src_key

        #if trainId is not None:
            #mask = self.getMask(runNo, src_key)
            #bg = self.getBackground(runNo, src_key, mask)
            #img = data[src_key].train_from_id(int(trainId))[1]*mask
            
            #img = self.cm_correction_horizontal(img)
            #print("img avval", np.nanmean(img))
            #return img-bg
            ##return data[src_key].train_from_id(int(trainId))[1]
        
        #if src_key is not None:
            #data = data[src_key]

        

        #return data
        
    def get_train_ids(self):
        return self.data.train_ids
    
    

    #def chunks(self, trains_per_part, src_key):
        #"""
        #This function will yield chunks from run number runNo, from the instrument-configuration
        #in ``src_key``, it can optionally be passed a selection to constrain the returned trains.

        #If ``src_key`` is ``False``, the default source set by setDefaultSource() is used.
        #"""        
        #chunks = self.data.split_trains(trains_per_part=trains_per_part)
        #for chunk in chunks:
            #yield np.array(chunk.get_array(*src_key))

    def getChunk(self, chunkid, src_key):
        return self.datachunks[chunkid][src_key].xarray()

    def getData(self, trainId,src_key):
        return self.data[src_key].train_from_id(trainId)
    
    def getAllData(self, src_key):
        data=self.data[src_key].xarray()
        return data.trainId, data.values
        
    def getPattern(self, trainId,  bgcorrection = True, staticmask = False, geometry=True ):
        img = np.array(self.data[self.pattern_src_key].train_from_id(trainId)[1], dtype=float)        
        return self.correctPattern(img,  bgcorrection = bgcorrection, staticmask = staticmask, geometry=geometry )
    
    def getTof(self,trainId):
        tofdata = np.array(self.data[self.tof_src_key].train_from_id(trainId)[1], dtype=float) 
        return self.correctTof(tofdata)
    
    def correctPattern(self, img, bgcorrection = True, staticmask = False, geometry=True ):
        img = self.cm_correction_vertical(self.cm_correction_horizontal(img, self.mask), self.mask)        
        if bgcorrection:
            img -= self.bg
            img[img<0]=0
        if staticmask and self.maskstatic is not None:
            img = img*self.maskstatic
        else:
            img = img*self.mask
        #plt.imshow(np.log(np.abs(img)))
        #plt.show()
              
        if geometry:
            img = self.applyGeometry(img)
        #print(img.shape, img.dtype)
        return img
        
    def correctTof(self, data):    
        data = -(data-np.mean(data[:2000]))
        return data
    
    #def getIdsChunk(self, chunkid=0):
        #return self.datachunks[chunkid][self.pattern_src_key].train_ids
    
    #def getPatternsChunk(self,chunkid=0):
        ##imgs = np.array(self.datachunks[chunkid][self.pattern_src_key], dtype=float) 

        #imgs = self.datachunks[chunkid].get_array(*self.pattern_src_key)
        
        #print(imgs.shape)
        
        ##plt.imshow(imgs[0,:,:])
        ##plt.show()
        
        #corrected_imgs= []
        
        #for i in range(imgs.shape[0]):
            #corrected_imgs.append(self.correctPattern(np.array(imgs[i,:,:], dtype=float)))
        
        #return np.array(corrected_imgs)
    

    def getPatternsChunk(self,chunkid=0, staticmask=False):
        #imgs = np.array(self.datachunks[chunkid][self.pattern_src_key], dtype=float) 

        data = self.getChunk(chunkid, self.pattern_src_key)
        
        imgs = np.array(data.values, dtype=float)
        pids = np.array(data.trainId)
        
        #print(imgs.shape)
        
        #plt.imshow(imgs[0,:,:])
        #plt.show()
        
        corrected_imgs= []
        
        for i in range(imgs.shape[0]):
            corrected_imgs.append(self.correctPattern(np.array(imgs[i,:,:], dtype=float), staticmask=staticmask))
        
        return np.array(corrected_imgs), pids



    def getTofChunk(self,chunkid=0):
        #imgs = np.array(self.datachunks[chunkid][self.pattern_src_key], dtype=float) 

        data = self.getChunk(chunkid, self.tof_src_key)
        
        traces = np.array(data.values, dtype=float)
        pids = np.array(data.trainId)
        
        corrected_traces = []
        
        for i in range(traces.shape[0]):
            corrected_traces.append(self.correctTof(np.array(traces[i], dtype=float)))
        
        return np.array(corrected_traces), pids



    def getMask(self):
        """
        Compute mask from the first chunk of mask data trains.
        """
        # We use 'next' to only use the first chunk of images.
        print("Computing mask")
        #images = self.datachunks[0].get_array(*self.pattern_src_key)
        data = self.getChunk(0,self.pattern_src_key)
        images = np.array(data.values, dtype=float)
        sums = np.nansum(images, axis=(1,2))
        sortedind = np.argsort(sums)
        
        print(sums.shape, images.shape)
        images = images[np.argsort(sums)[:images.shape[0]//2],:,:]
        avg = images.mean(axis=0)
        std = images.std(axis=0)
        avgToStd = avg/std
        mask = np.ones_like(avg)
        mask[avg > self.maskparam["maskAvg"]] = np.NAN
        mask[std > self.maskparam["maskStd"]] = np.NAN
        mask[std <= self.maskparam["maskMinStd"]] = np.NAN
        mask[avgToStd > self.maskparam["maskAvgToStd"]] = np.NAN
        #maskBig = np.copy(mask)
        #maskBig = check8neighValue(maskBig)
        #maskBig[np.isnan(mask)] = np.NAN
        
        #plt.imshow(mask)
        #plt.show() 

        #mask[ 490:534,482:542]=np.NAN
        mask[ 489:535,481:543]=np.NAN

         #[482,490], [542,534]

        return mask 




    def getBackground(self, mask):
        """
        Compute mask from the first chunk of mask data trains.
        """
        # We use 'next' to only use the first chunk of images.
        print("Computing background")
        
        #images = np.array(self.datachunks[0].get_array(*self.pattern_src_key), dtype=float)
        data = self.getChunk(0,self.pattern_src_key)
        images = np.array(data.values, dtype=float)
        
        print("Computing bg cm")
        sums = []   
        
        for i in range(images.shape[0]):        
            tempimg = self.cm_correction_vertical(self.cm_correction_horizontal(images[i,:,:], mask), mask)
            sums.append(np.nanmean(tempimg))
            images[i,:,:] = tempimg
        #print(sums)
        bg = np.nanmean(images[np.argsort(sums)[:int(images.shape[0]*0.5)],:,:], axis=0)
        print("Bg shape", bg.shape)
        
        
        return bg 




    def cm_correction_horizontal(self,img, mask):
        """
        Horizontal common mode correction.

        The horizontal median is subtracted row by row and
        independently for the left and right half of the image.
        """
        
        maskedimg = img*mask
        #maskedimg[maskedimg==0]=np.nan
        img[:,:512] -= np.nanmedian(maskedimg[:,:64], 1).reshape(-1, 1)
        img[:,512:] -= np.nanmedian(maskedimg[:,-64:], 1).reshape(-1, 1)
        return img

    def cm_correction_vertical(self, img, mask):    
        """
        Vertical common mode correction.

        The vertical median is subtracted column by column and
        independently for the top and bottom half of the image.
        """
        #maskedimg = img*mask
        #vals = maskedimg[:512,:]
        #vals = np.sort(vals[~numpy.isnan(x)])
        #if vals.shape[0]>50:
            #lim = np.amax([vals.shape[0]//5, 10]])
        #else:
            #lim = vals.shape[0]
        #img[:512,:] -= np.nanmedian(vals[:lim], 0)
        maskedimg = img*mask
        #maskedimg[maskedimg==0]=np.nan

        img[:512,:] -= np.nanmedian(maskedimg[:64,:], 0)  
        img[512:,:] -= np.nanmedian(maskedimg[-64:,:], 0)
        
        return img

    #def cm_correction_horizontal(self,img, mask):
        #"""
        #Horizontal common mode correction.

        #The horizontal median is subtracted row by row and
        #independently for the left and right half of the image.
        #"""
        
        #maskedimg = img*mask
        #img[:,:512] -= np.nanmedian(maskedimg[:,:512], 1).reshape(-1, 1)
        #img[:,512:] -= np.nanmedian(maskedimg[:,512:], 1).reshape(-1, 1)
        #return img

    #def cm_correction_vertical(self, img, mask):
        #"""
        #Vertical common mode correction.

        #The vertical median is subtracted column by column and
        #independently for the top and bottom half of the image.
        #"""
        #maskedimg = img*mask

        #img[:512,:] -= np.nanmedian(maskedimg[:512,:], 0)
        #img[512:,:] -= np.nanmedian(maskedimg[512:,:], 0)
        #return img
   
    #def cm_correction_horizontal(self,img):
        #"""
        #Horizontal common mode correction.

        #The horizontal median is subtracted row by row and
        #independently for the left and right half of the image.
        #"""
        #img[:,:512] -= np.nanmedian(img[:,:512], 1).reshape(-1, 1)
        #img[:,512:] -= np.nanmedian(img[:,512:], 1).reshape(-1, 1)
        #return img

    #def cm_correction_vertical(self, img):
        #"""
        #Vertical common mode correction.

        #The vertical median is subtracted column by column and
        #independently for the top and bottom half of the image.
        #"""
        #img[:512,:] -= np.nanmedian(img[:512,:], 0)
        #img[512:,:] -= np.nanmedian(img[512:,:], 0)
        #return img
    
    def getGeometry(self):
        
        pnccdpos = {}
        for key in self.pnccdpos_src_key.keys():
            pnccdpos[key] = self.data[self.pnccdpos_src_key[key]].train_from_index(0)[1]
        
        print(pnccdpos)
        
        #gapAdd=1.3
        #moveAdd=0.
        #closedPnccdUp = 16.32
        #closedPnccdDown = 18.56
        ##rightStart = 0 #.25  #15.98414063
        ##leftStart = 0 #21.10546875
        #rightStart = 15.98414063
        #leftStart = 21.10546875
       
        self.detdistance =  350 - (pnccdpos["pnccdFEL"]-18.87)
        self.gapInMm =pnccdpos["pnccdUp"] + pnccdpos["pnccdDown"]- self.pnccdoffsets["closedPnccdUp"] - self.pnccdoffsets["closedPnccdDown"]  + self.pnccdoffsets["gapAdd"]
        gap_config = {'gap': self.gapInMm *10**(-3) , 
                      'top_offset': ((pnccdpos["pnccdRight"] - self.pnccdoffsets["rightStart"])*10**(-3), 0, 0.0), 
                      'bottom_offset': (-(pnccdpos["pnccdLeft"]  - self.pnccdoffsets["leftStart"]- self.pnccdoffsets["moveAdd"])*10**(-3), 0, 0.0)}
        self.geom = PNCCDGeometry.from_relative_positions(**gap_config)   
        self.pnccdpos = pnccdpos
        
    def get_center_pos(self, center):
        pixsize=0.075
        delta_v = self.pnccdpos["pnccdUp"]-self.pnccdoffsets["closedPnccdUp"]
        alpha = (self.pnccdpos["pnccdRight"] - self.pnccdoffsets["rightStart"]) + (self.pnccdpos["pnccdLeft"]  - self.pnccdoffsets["leftStart"])+ self.pnccdoffsets["moveAdd"]
        if alpha<0:
            alpha=0
        delta_h = self.pnccdpos["pnccdRight"] - self.pnccdoffsets["rightStart"]
        
        yval = (512-center[1])*pixsize + delta_v        
        xval = center[0]*pixsize+delta_h - alpha
        
        return [xval, yval]
    
    def get_center_coords(self, center):
        pixsize=0.075
        delta_v = self.pnccdpos["pnccdUp"]-self.pnccdoffsets["closedPnccdUp"]
        alpha = (self.pnccdpos["pnccdRight"] - self.pnccdoffsets["rightStart"]) + (self.pnccdpos["pnccdLeft"]  - self.pnccdoffsets["leftStart"])+ self.pnccdoffsets["moveAdd"]
        if alpha<0:
            alpha=0
        delta_h = self.pnccdpos["pnccdRight"] - self.pnccdoffsets["rightStart"]

        nx = (center[0]-delta_h+alpha)/pixsize
        ny = 512+(delta_v-center[1])/pixsize
        return [int(np.round(nx)), int(np.round(ny))]



        
    #def getGeometry(self):
        
        #pnccdpos = {}
        #for key in self.pnccdpos_src_key.keys():
            #pnccdpos[key] = self.data[self.pnccdpos_src_key[key]].train_from_index(0)[1]
        
        #print(pnccdpos)
        #gapAdd=0
        #moveAdd=0
        #closedPnccdUp = 16.32
        #closedPnccdDown = 18.56
        ##rightStart = 0 #.25  #15.98414063
        ##leftStart = 0 #21.10546875
        #rightStart = 15.98414063
        #leftStart = 21.10546875
        #offSet = 1.3
        #gapInMm = pnccdpos["pnccdUp"] + pnccdpos["pnccdDown"]- closedPnccdUp - closedPnccdDown + offSet + gapAdd
        
        #gap_config = {'gap': gapInMm *10**(-3) , 'top_offset': ((pnccdpos["pnccdRight"] - rightStart)*10**(-3), 0, 0.0), 'bottom_offset': ((pnccdpos["pnccdLeft"]  - leftStart)*10**(-3) + moveAdd, 0, 0.0)}
        #self.geom = PNCCDGeometry.from_relative_positions(**gap_config)
        
    def applyGeometry(self, img):
        return self.geom.position_modules(img)[0]

        #pnccdUp = 
        #pnccdDown = self.data[('SQS_NQS_PNCCD/MOTOR/PNCCD_UP','actualPosition.value')].train_from_id(trainId))
        
        
        
        
#def addGap(img, pnncd_up, pnccd_down, pnccd_right, pnccd_left, gapAdd=0, moveAdd=0):
    #closedPnccdUp = 16.32
    #closedPnccdDown = 18.56
    #rightStart = 15.98414063
    #leftStart = 21.10546875
    #offSet = 1.2
    #gapInMm = pnncd_up + pnccd_down- closedPnccdUp - closedPnccdDown + offSet + gapAdd
    #GAP_CONFIG = {'gap': gapInMm *10**(-3) , 'top_offset': ((pnccd_right - rightStart)*10**(-3), 0, 0.0), 'bottom_offset': ((pnccd_left - leftStart)*10**(-3) + moveAdd, 0, 0.0)}
    #geom = PNCCDGeometry.from_relative_positions(**GAP_CONFIG)
    #return geom.position_modules(img)[0]


#def getMean(RunNr, srcKey):
    #'''
    #Compute average 
    #Exclude highest and lowest 10 vals. Dont use sorting nor store all values
    #'''
    
    #getData = GetData()

    #trainsPerPart = 1000
    #nImages = 0
    #cutNr = 10
    #start = True
    #totImages = 0
##     highImage = np.zeros(cutNr, imageShape[1], imageShape[2])
##     lowImage = np.zeros(cutNr, imageShape[1], imageShape[2])

    #sumImage = []
    
    #for images in getData.chunks(RunNr, trainsPerPart, src_key=srcKey):
        #images = np.transpose(images, axes=[1, 2, 0])
        #imageShape = images.shape[:-1]
        
        #if start:
            #sumImage = np.zeros(imageShape)
            #start = False
        
        #nImages += images.shape[2]
        #print('here')
        #print(imageShape)
        #print(nImages)
        #localsum = np.array(np.sum(images, axis=2))
        #print(localsum.shape)
        #sumImage += localsum
        ##if develop and nImages > 20000:
            ##return sumImage/nImages, highImage, lowImage, nImages
    ##if develop:
        ##return sumImage/nImages, highImage, lowImage, nImages
            
    #return sumImage/nImages

##def getMean(RunNr, srcKey):
    ##'''
    ##Compute average 
    ##Exclude highest and lowest 10 vals. Dont use sorting nor store all values
    ##'''
    
    ##getData = GetData()

    ##trainsPerPart = 1000
    ##nImages = 0
    ##cutNr = 10
    ##start = True
###     highImage = np.zeros(cutNr, imageShape[1], imageShape[2])
###     lowImage = np.zeros(cutNr, imageShape[1], imageShape[2])

    
    ##for images in getData.chunks(RunNr, trainsPerPart, src_key=srcKey):
        ##images = np.transpose(images, axes=[1, 2, 0])
        ##imageShape = images.shape[:-1]
        ##if start:
            ##sumImage = np.zeros(imageShape)
            ##highImage = images[:, :, :cutNr]
            ##lowImage = images[:, :, :cutNr]
            ##images = images[:, :, cutNr:]
            ##start = False
        ##print('here')
        ##print(imageShape)
        ###combine the 10 lowest values per pixel with the new images. 
        ##allLowImage = np.concatenate((lowImage, images), axis=2)
        ##r, c, i = allLowImage.shape
        ##print(i)
        ##remainLength = i-cutNr
        ###keep these images. These might be images, that never existed. but for averaging, this is not a problem
        ###use partition as it only partly sorts the data, such that only the cutoff value and everythin above is correct. dont care for the order within, as it gets averaged anyways. Save time.
        ##keepImage = np.partition(allLowImage, -remainLength)[:, :, -remainLength:]
        ###create new matrix with 10 lowest values. kicking out the highest values per pixel.
        ##lowImage = np.partition(allLowImage, cutNr)[:, :, :cutNr]
        ###repeat same in the other direction with the already new images:
        ##allHighImage = np.concatenate((highImage, keepImage), axis=2)
        ##keepImage = np.partition(allHighImage, remainLength)[:, :, :remainLength]
        ##highImage = np.partition(allHighImage, -cutNr)[:, :, -cutNr:]
        
        ##sumImage += np.sum(keepImage, axis=2)
        ##nImages += remainLength
        ##print(nImages)
        ###if develop and nImages > 20000:
            ###return sumImage/nImages, highImage, lowImage, nImages
    ###if develop:
        ###return sumImage/nImages, highImage, lowImage, nImages
            
    ##return sumImage/nImages

#SRC = {
#'pnccdUp' : ('SQS_NQS_PNCCD/MOTOR/PNCCD_UP','actualPosition.value'),
#'pnccdDown' : ('SQS_NQS_PNCCD/MOTOR/PNCCD_DOWN','actualPosition.value'),
#'pnccdLeft' : ('SQS_NQS_PNCCD/MOTOR/PNCCD_RIGHT','actualPosition.value'),
#'pnccdRight' : ('SQS_NQS_PNCCD/MOTOR/PNCCD_LEFT','actualPosition.value'),
#'pnccdImage' : ('SQS_NQS_PNCCD1MP/CAL/PNCCD_FMT-0:output', 'data.image'),}
##getData.setFilterSources(SRC.values())



#meanpatt = getMean(300, SRC['pnccdImage'])


#plt.imshow(meanpatt)
#plt.show()








