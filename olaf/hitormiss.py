
import h5py
import glob
import numpy as np
import matplotlib.pyplot as plt
from multiprocessing import Pool, Queue

def hitormiss_thresh(patterns, settings):
    
    print("Computing hit-or-miss threshold")
    
    intensities = []

    for i in range(patterns.shape[0]):        
        intensities.append(np.nansum(patterns[i,:,:]))
    print(intensities)

    intensities = np.array(intensities)
    srtindexes = np.argsort(intensities)
    sortedint = intensities[srtindexes]

    totalshots = len(intensities)


    lowermean = np.mean(sortedint[:int(len(intensities) * settings["miss_fraction"])])
    std = np.std(sortedint[:int(len(intensities) * settings["miss_fraction"])])
    threshhitormiss = lowermean + settings["miss_std"] * std
    

    return threshhitormiss

