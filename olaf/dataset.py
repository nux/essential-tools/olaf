import numpy as np
import h5py
import os
import mmap
from .shot import Shot
import json
# from .h5tools import load_features_from_hdf5
#from .sftools import load_features_from_sf, check_pids
from .h5tools import load_metadata_from_hdf5

from .jsontools import list_to_json
from .parser import Parser
from .euxfel import RunLoader
from collections.abc import Iterable

import importlib
from inspect import getmembers, isfunction
from PIL import Image
from multiprocessing import Pool, Queue
import time
import signal
import prettytable
import glob
import re
import sys
import io
from .hitormiss import hitormiss_thresh #hitormiss, hitormiss_tof
import matplotlib

#matplotlib.use("agg")
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_style("white")

analysis_functions = None

save_stdout = None
save_stderr = None
verbose = False


def redirect():
    global save_stdout, save_stderr
    save_stdout = sys.stdout
    #save_stderr = sys.stderr

    if not verbose:
        # sys.stdout = io.BytesIO()
        sys.stdout = io.StringIO()
        #sys.stderr = io.StringIO()


def unredirect():
    global save_stdout, save_stderr
    
    if not verbose:
        #sys.stderr = save_stderr
        sys.stdout = save_stdout

#def redirect():
    #return


#def unredirect():
    #return

def load_functions(settings):
    global analysis_functions

    if analysis_functions is None:
        analysis_functions = load_functions_from_file(
            os.path.join(settings["workdir"], settings["shot_analysis"]["filename"]))


def get_data_slice(h5data, ishot):
    h5slice = {}
    for key in h5data.keys():
        h5slice[key] = h5data[key][ishot]
        # print(key, h5slice[key].shape)
    return h5slice


def load_functions_from_file(filepath):
    print("Loading", filepath)
    analysis_functions = []

    spec = importlib.util.spec_from_file_location("module.name", filepath)
    foo = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(foo)
    funcs = getmembers(foo, isfunction)

    for func in funcs:
        if not func[0].startswith("_"):
            analysis_functions.append(func[1])

    return sorted(analysis_functions, key=lambda x: x.__code__.co_firstlineno)


def get_shot_analysis(settings, run, index):
    global analysis_functions, verbose

    redirect()

    runloader = None
    
    if settings["tof_analysis"] == 1:
        runloader = RunLoader(settings["proposal"], run, settings['channels']["pattern"], settings['channels']["tof"], settings["cache"], chunksize = settings["chunksize"])
    else:
        runloader = RunLoader(settings["proposal"], run, settings['channels']["pattern"], None, settings["cache"], chunksize = settings["chunksize"])
        
    nworkers = settings["workers"]
    nchunks = len(runloader.datachunks)
    
    staticmaskflag=False
    
    analysis = []
    for ichunk in range(index, nchunks, nworkers):
    
        analysis_chunk = []
        #patterns, pids = runloader.getIdsChunk(ichunk)
        patterns, pids = runloader.getPatternsChunk(ichunk, staticmask=staticmaskflag)
        traces = None
        tofpids = None
        if settings["tof_analysis"] == 1:
            traces, tofpids = runloader.getTofChunk(ichunk)
        
        mask = np.ones(patterns[0,:,:].shape)
        mask[np.isnan(patterns[0,:,:])] = 0

        print("Worker", index, "chunk", ichunk, "pattern size", pids.shape,  patterns.shape)#, "Tof size", tofpids.shape,  traces.shape)


        
        for ishot in range(len(pids)):
            shot = Shot(settings["shot_analysis"], pids[ishot], {})
            
            datadict = {"pattern" : patterns[ishot],
                           "mask":     mask}
            if traces is not None:
                datadict["tof"] = traces[ishot]
            
            shot.set_data(datadict)
            shot.analyze(analysis_functions)

            analysis_chunk.append(shot.get_properties())
            
        hitsum = np.sum([x['hit'] for x in analysis_chunk])
        hitrate = hitsum/len(pids)
        
        for ishot in range(len(pids)):
            analysis_chunk[ishot]['hitrate'] = hitrate
            
        analysis = analysis+analysis_chunk
        
        
    unredirect()
    return analysis


def init_worker(settings, inverbose):
    global verbose

    verbose = inverbose

    signal.signal(signal.SIGINT, signal.SIG_IGN)
    # args=q.get()
    # np.random.seed(seed)
    load_functions(settings)

    # import os
    # print ("Process {0} init with arguments {1}".format(os.getpid(),settings))


# def analyze_subset(shots):


class Dataset:
    def __init__(self, path, settings, workers=1, inverbose=False):
        global verbose

        verbose = inverbose
        self.settings = settings
        self.path = path
        # self.file_obj=[]
        self.incomplete_pids=0
        self.files = []
        self.analysis = Parser(self.settings["workdir"])
        self.mask = []
        self.h5file = {}

        #self.load_mask()
        load_functions(self.settings)

        self.workers = workers
        # self.pool = Pool(processes=workers)
        self.pool = Pool(processes=workers, initializer=init_worker, initargs=(self.settings, inverbose))

    def delete(self):
        self.pool.terminate()
        self.pool.join()

        self.pool.close()

    def load_files(self, filename, run):
        self.timing = {}

        self.filename = filename
        self.run = run
        # self.file_obj=[]
        self.files = []
        self.analysis = Parser(self.settings["workdir"])
        self.h5file = {}

        self.load_data()
        
        
        self.global_properties = {
            "path": self.path,
            "filename": self.filename,
            "run": self.run
        }

        
        

    def load_mask(self):

        img = Image.open(os.path.join(self.settings["workdir"], self.settings["mask"])).convert('L')
        mask = np.array(img, dtype=float)
        mask = mask / np.amax(mask)
        mask[mask < 0.5] = 0
        mask[mask >= 0.5] = 1

        # print(mask.shape)
        self.mask = mask

    def load_data(self):

        redirect()

        wholefilename = os.path.join(self.settings["workdir"], self.path, self.filename)

        #print(wholefilename)
        runloader = None
        
        if self.settings["tof_analysis"] == 1:
            runloader = RunLoader(self.settings["proposal"], self.run, self.settings['channels']["pattern"], self.settings['channels']["tof"], self.settings["cache"], chunksize = self.settings["chunksize"])
        else:
            print("TOF analysis excluded")
            runloader = RunLoader(self.settings["proposal"], self.run, self.settings['channels']["pattern"], None, self.settings["cache"], chunksize = self.settings["chunksize"])
            
        #print(runloader.get_train_ids())
        print(len(runloader.get_train_ids()),"total pids")
        print(len(list(runloader.datachunks)),"total chunks")
        #print(runloader.getPatternsChunk().shape)
        # pids, filetotshots, indices, hitrate = hitormiss_functions[0](wholefilename, self.settings["hitormiss"], self.pool)
        t0 = time.time()
        
        if self.settings["hitormiss"]["img_thres"]<=0:
            self.settings["shot_analysis"]["hit_thresh"] = hitormiss_thresh(runloader.getPatternsChunk()[0], self.settings["hitormiss"])
            print("hit-or-miss threshold set to", self.settings["shot_analysis"]["hit_thresh"])
        else:
            self.settings["shot_analysis"]["hit_thresh"] = self.settings["hitormiss"]["img_thres"] 

        t1 = time.time()
        tottime = t1 - t0
        self.timing["Hit or Miss"] = tottime
        unredirect()


    def analyze(self):

        redirect()

        t0 = time.time()


        params = []

        for i in range(self.workers):
            params.append([self.settings, self.run, i])
        
        shots_analysis_temp = self.pool.starmap(get_shot_analysis, params)



        self.shots_analysis = []
        
        for i in range(0, len(shots_analysis_temp)):
            self.shots_analysis = self.shots_analysis + shots_analysis_temp[i]


        for i in range(0, len(self.shots_analysis)):
            self.shots_analysis[i] = {**self.global_properties, **self.shots_analysis[i]}


        #print(self.shots_analysis)

        #exit(0)
        t1 = time.time()
        tottime = t1 - t0
        self.timing["Shot Analysis"] = tottime

        unredirect()


    def print_timing(self):
        nshots = len(self.shots_analysis)
        if nshots > 0:
            
            names = []
            seconds = []
            for key in self.timing.keys():
                names.append(key)
                seconds.append(self.timing[key])

            names.append("Total")
            seconds.append(np.sum(np.array(seconds)))

            secperhit = []
            hitspersec = []
            shotspersec = []
            for ss in seconds:
                secperhit.append(ss / nshots)
                hitspersec.append(nshots / ss)
                shotspersec.append(nshots / ss)

            tab = prettytable.PrettyTable()
            # tab.set_cols_align(["l", "r", "r", "r"])
            # tab.set_chars([None,"|","","-"])
            # tab.set_style(float_format=".3f")

            tab.field_names = ["Phase", "Seconds", "Seconds per Hit", "Hits per Second", "Shots per Second"]

            seconds = ["{:.2f}".format(x) for x in seconds]
            hitspersec = ["{:.1f}".format(x) for x in hitspersec]
            secperhit = ["{:.3f}".format(x) for x in secperhit]
            shotspersec = ["{:.1f}".format(x) for x in shotspersec]

            for row in zip(names, seconds, secperhit, hitspersec, shotspersec):
                tab.add_row(row)

            print("Run",self.run, " - Analyzed patterns:", nshots)          
            print(tab)
        else:
            print("No shots, no time")


    def save(self):
        filename = "analysis_{:04d}".format(self.run) + "_" + self.settings[
            "version"]
        self.analysis.save(self.shots_analysis,
                           os.path.join(self.settings["workdir"], self.settings["outdir"], filename + ".csv"), mode='w')

    def check_existing_runs(self):
        
        existing = os.listdir(os.path.join(self.settings["workdir"], self.settings["outdir"]))
        runslist = []
        # print(existing)
        for file in existing:

            match = re.search(r'analysis_(\d\d\d\d)_' + self.settings["version"] + '.csv', file)
            # If-statement after search() tests if it succeeded
            if match:
                #print('found', match.group(1))  ## 'found word:cat'
                runslist.append(int(match.group(1)))
            # else:
            # print('did not find')
        #print(runslist)
        return runslist
