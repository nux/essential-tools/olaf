import numpy as np
from .jsontools import json_to_list, list_to_json
from .csvtools import csv_to_list, list_to_csv

# from .h5tools import load_slice_from_hdf5, load_metadata_from_hdf5
#from .sftools import load_features_from_sf

from .proptools import merge_dict, print_dict, flatten_dict, get_keys_from_path, get_path_from_keys
import os
import copy
from functools import reduce  # forward compatibility for Python 3
import operator
from typing import Union
import operator
import stat


def get_truth(inp, relate, cut):
    rel_ops = {
        '>': operator.gt,
        '<': operator.lt,
        '>=': operator.ge,
        '<=': operator.le,
        '==': operator.eq,
        '!=': operator.ne
    }
    return rel_ops[relate](inp, cut)


def getFromDict(dataDict, mapList):
    return reduce(operator.getitem, mapList, dataDict)


def get_property(item, keys):
    # print(item)

    if keys[0] in item.keys():
        if len(keys) == 1:
            return item[keys[0]]
        else:
            return get_property(item[keys[0]], keys[1:])
    else:
        return None


# def filter_by_range(analysis, keys, condition):
#     filtered_analysis = []
#     for item in analysis:
#         valid = 1
#         prop = get_property(item, keys)
#         if prop == None:
#             valid = 0
#     else:
#         if condition[0] != None:
#             if prop < condition[0]:
#                 valid = 0
#
#         if condition[1] != None:
#             if prop > condition[0]:
#                 valid = 0
#     if valid:
#         filtered_analysis.append(item)
#
#     return filtered_analysis
#
#
# def filter_by_value(analysis, keys, condition):
#     filtered_analysis = []
#     for item in analysis:
#         prop = get_property(item, keys)
#         if prop != None:
#             if prop == condition:
#                 filtered_analysis.append(item)
#
#     return filtered_analysis


class Parser:
    def __init__(self, workdir: str = "/"):
        '''Parser constructor.
        
        :param workdir: root path for the files. All file paths have to be expressed relatively to this path
        '''

        self.workdir = workdir

    def load(self, filename: str, append: bool = False) -> list:
        '''Load an analysis file and return it. The format of the returned object is a list of (nested) dictionaries, where each dictionary corresponds to the analysis of a shot
        
        :param filename: analysis file (json or csv format)
        
        :param append: If True, append the filename to the workdir path given during the Parser initialization.
        
        :return: list of (nested) dictionaries, where each dictionary corresponds to the analysis of a shot
        '''

        locwdir = ""
        if append:
            locwdir = self.workdir

        extension = os.path.splitext(filename)[1][1:]
        if extension == "json":
            return json_to_list(os.path.join(locwdir, filename))
        elif extension == "csv":
            return csv_to_list(os.path.join(locwdir, filename))
        else:
            print("ERROR: file format ", extension, " not recognized.")
            return []

    def save(self, analysis: list, filename: str, mode: str = 'w'):
        '''Save the analysis to disk.
        
        :param analysis: dictionaries containing the shot-by-shot analysis

        :param filename: analysis file (json or csv format) where the analysis is saved
        
        :param mode: "w" (overwrite if already exists) or "a" (append if already exists)
        '''

        extension = os.path.splitext(filename)[1][1:]
        if extension == "json":
            list_to_json(os.path.join(self.workdir, filename), analysis, mode=mode)
        elif extension == "csv":
            list_to_csv(os.path.join(self.workdir, filename), analysis, mode=mode)
        else:
            print("ERROR: file format ", extension, " not recognized.")
            return 
        
        os.chmod(os.path.join(self.workdir, filename), 0o777)

    def get_properties(self, analysis: list) -> list:
        '''Get the list of all properties present within a given dataset
        
        :param analysis: dictionaries containing the shot-by-shot analysis

        :return: list of available properties. "Nested" properties are separated by :code:"/"
        '''

        proplist = {}

        for index, prop in enumerate(analysis):
            proplist = merge_dict(proplist, prop, index)

        flattened = flatten_dict(proplist)

        return list(flattened.keys())

    def get_data(self, analysis: Union[dict, list], channel: str):
        '''Read the data from the experimental h5 file.
        
        :param analysis: dictionaries containing the shot-by-shot analysis

        :param channel: channel of the data within the experimental h5 file

        :return: the data
        '''

        h5data = []

        analysisloc = []
        if isinstance(analysis, list):
            analysisloc = analysis
        else:
            analysisloc = [analysis]

        # pids = np.array([item["pid"] for item in analysisloc])

        h5data = []
        channelnames = {"required": channel}

        # we iterate over filenames instead of single shots
        # first, get the unique filenames
        unique_fn = list(set([x["filename"] for x in analysisloc]))

        # Then iterate over them
        for fn in unique_fn:
            # get all the pids for that file
            pids = []
            for x in analysisloc:
                if x["filename"] == fn:
                    pids.append(x["pid"])
            # get the images from that file
            h5data.append(load_features_from_sf(fn, channelnames, np.array(pids))[0]["required"])

        # for item in analysisloc:
        #     pid = item["pid"]
        #     filename = os.path.join(self.workdir, item["path"], item["filename"])
        #     h5data.append(load_features_from_sf(filename, channelnames, np.array([pid]))[0]["required"][0])
        # print("h5data shape", np.shape(h5data))
        if isinstance(analysis, list):
            return h5data
        else:
            return h5data[0]

    def get_value(self, analysis: Union[dict, list], key: Union[str, list]) -> Union[dict, list]:
        '''Return the value or a list of values for the given marker key
        
        :param analysis: dictionary (for a single shot) or list of dictionaries (for multiple shots) containing the shot-by-shot analysis

        :param key: key of interest. Nested keys can be expressed in two different ways, or as a list (["key", "subkey", "subsubkey"] or as a single string ("key/subkey/subsubkey")         
        
        :return: the value (if analysis is a single dictionary) or list of values (if analysis is a list of dictionaries)
        '''
        keys_loc = []
        if isinstance(key, list):
            keys_loc = key
        else:
            keys_loc = get_keys_from_path(key)

        if isinstance(analysis, list):
            return [get_property(item, keys_loc) for item in analysis]

        else:
            return get_property(analysis, keys_loc)

    def filter(self, analysis: list, key: Union[str, list], condition: str, value) -> list:
        '''Filter the analysis basing on a given condition
        
        :param analysis: list of dictionaries containing the shot-by-shot analysis to be filtered

        :param key: key on which the analysis is filtered. Nested keys can be expressed in two different ways, or as a list (["key", "subkey", "subsubkey"] or as a single string ("key/subkey/subsubkey")         
        
        :param condition: relation basing on which the data is filtered. 
                        Condition can be:
                        
                        - :code:`==` equality
                        - :code:`!=` inequality
                        - :code:`>` greater 
                        - :code:`>=` greater or equal
                        - :code:`<` less
                        - :code:`<=` less or equal 
        
        :param value: value with which the analysis is compared basing on the given condition
        
        :return: the filtered analysis
        '''

        keys_loc = []
        if isinstance(key, list):
            keys_loc = key
        else:
            keys_loc = get_keys_from_path(key)

        filtered_analysis = []
        for item in analysis:
            prop = get_property(item, keys_loc)
            if prop != None:
                if get_truth(prop, condition, value):
                    filtered_analysis.append(copy.deepcopy(item))

        return filtered_analysis

    def sort(self, analysis: list, key: Union[str, list]) -> list:
        '''Sort the analysis basing on the value stored in the given key. The values stored at the given key must be objects that can be sorted (like numbers). The analysis is sorted by increasing values.
        
        :param analysis: list of dictionaries containing the shot-by-shot analysis to be filtered

        :param key: key on which the analysis is filtered. Nested keys can be expressed in two different ways, or as a list (["key", "subkey", "subsubkey"] or as a single string ("key/subkey/subsubkey")         
        
        :return: the sorted analysis
        '''

        keys_loc = []
        if isinstance(key, list):
            keys_loc = key
        else:
            keys_loc = get_keys_from_path(key)

        newanalysis = copy.deepcopy(analysis)

        srtf = lambda item: get_property(item, keys_loc)

        analysis = sorted(analysis, key=srtf)
        return analysis
