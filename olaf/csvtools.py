import csv
from .proptools import merge_dict, flatten_dict, unflatten_dict
import os

def list_to_csv(filename, props, mode='a'):
    #flatten_proplist=None
    header=None
    write_header=None
    if os.path.exists(filename) and mode=='a':
        with open(filename, newline='') as csvfile:
            reader = csv.DictReader(csvfile, quoting=csv.QUOTE_NONNUMERIC)
            
            for irow, row in enumerate(reader):
                header=row.keys()    
                break

    else:
        proplist={}
        for index,prop in enumerate(props):
        #proplist={**proplist, **prop}
            proplist=merge_dict(proplist,prop, index)
        
        #for prop in props:
        flatten_proplist=flatten_dict(proplist)
        header=flatten_proplist.keys()
        write_header=True

    with open(filename, mode, newline='') as csvfile:
        
        writer = csv.DictWriter(csvfile, fieldnames=header, quoting=csv.QUOTE_NONNUMERIC)

        if write_header:
            writer.writeheader()
        for prop in props:
            flattenprop = flatten_dict(prop)
            #print(flattenprop)
            writer.writerow(flattenprop)


def csv_to_list(filename):
    props=[]
    with open(filename, newline='') as csvfile:
        reader = csv.DictReader(csvfile, quoting=csv.QUOTE_NONNUMERIC)
        for row in reader:
            props.append(unflatten_dict(row))
            
            #print(row['first_name'], row['last_name'])
    #print(props)
    return props

    #with open(filename, mode) as f:
        #for prop in props:
            ##print(prop)
            #jsonstr = json.dumps(prop)
            #f.write(jsonstr + "\n")
    
    

#def json_to_list(filename):
    #props=[]
    #with open(filename) as f:
        #for jsonObj in f:
            #prop = json.loads(jsonObj)
            #props.append(prop)
            
    #return props
