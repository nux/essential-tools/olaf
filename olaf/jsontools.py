import json
import numpy as np


def check_type(obj):
    if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
                        np.int16, np.int32, np.int64, np.uint8,
                        np.uint16, np.uint32, np.uint64)):

        return int(obj)

    elif isinstance(obj, (np.float_, np.float16, np.float32, np.float64)):
        return float(obj)

    elif isinstance(obj, (np.complex_, np.complex64, np.complex128)):
        return {'real': obj.real, 'imag': obj.imag}

    elif isinstance(obj, (np.ndarray,)):
        return obj.tolist()

    elif isinstance(obj, (np.bool_)):
        return bool(obj)

    elif isinstance(obj, (np.void)): 
        return None

    else:
        return obj

def check_prop(prop):
    for key in prop.keys():
        prop[key] = check_type(prop[key] )

def list_to_json(filename, props, mode='a'):
    #print(props)
    with open(filename, mode) as f:
        for prop in props:
            #print(prop)
            check_prop(prop)
            jsonstr = json.dumps(prop)
            f.write(jsonstr + "\n")
    
    

def json_to_list(filename):
    props=[]
    with open(filename) as f:
        for jsonObj in f:
            prop = json.loads(jsonObj)
            props.append(prop)
            
    return props
