import numpy as np
import h5py
import os

class Shot:
    def __init__(self, settings, pid, file_properties):
        self.settings=settings
        self.pid=pid

        self.h5file=None
        self.properties={}
        self.properties={**self.properties,**file_properties}
        
        self.properties["pid"]=self.pid



    def set_data(self, h5file):
        self.h5file=h5file
        
            
    def set_property(self, key, value):
        self.properties[key]=value
        


    def analyze(self, analysis_functions):
        
        for func in analysis_functions:
            tempdict = func(self.settings, self.h5file, self.properties)
            
            if isinstance(tempdict, dict):
                for tempkey in tempdict.keys():
                    self.properties[tempkey]=tempdict[tempkey]
        
        
    def get_properties(self):
        
                
        return self.properties
        
