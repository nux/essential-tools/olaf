import yaml


def read_yaml(filename):
    data={}
    with open(filename, 'r') as stream:
        try:
            data=yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
            
    #print(data)
    return data
