from functools import reduce  # forward compatibility for Python 3
import operator
import numpy as np


def print_dict(nested_dictionary, tab=0, keysonly=False):
    for key, value in nested_dictionary.items():
        if type(value) is dict:
            print("  "*tab, key, ":")
            print_dict(value, tab+1, keysonly)
        else:
            if keysonly:
                print("  "*tab, key)                
            else:
                print("  "*tab, key, ":", value)


def merge_dict(dictlist,dict2, index):
    outdict=dictlist
    #print(outdict)
    for key, value in dict2.items():
        if (type(value) is dict):
            if key not in outdict.keys():
                outdict[key]={}
            outdict[key]=merge_dict(outdict[key],dict2[key], index)
            
        else:
            if key in outdict.keys():
                outdict[key].append(index)
            else:
                outdict[key]=[index]
    return outdict


def flatten_dict(dd, separator ='/', prefix =''):
    return { prefix + separator + k if prefix else k : v
             for kk, vv in dd.items()
             for k, v in flatten_dict(vv, separator, kk).items()
             } if isinstance(dd, dict) else { prefix : dd }



def unflatten_dict(dict1, separator="/"):
    result = {}
    for k, v in dict1.items():
          
        # for each key call method split_rec which
        # will split keys to form recursively 
        # nested dictionary
        split_rec(k, v, result, separator)
    return result
  
def split_rec(k, v, out, separator):
      
    # splitting keys in dict
    # calling_recursively to break items on '_'
    k, *rest = k.split(separator, 1)
    if rest:
        split_rec(rest[0], v, out.setdefault(k, {}), separator)
    else:
        out[k] = v


def get_keys_from_path(path, sep="/"):
    return path.split(sep)
    

def get_path_from_keys(keys, sep="/"):
    return sep.join(keys)    



