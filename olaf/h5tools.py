import numpy as np
import h5py
import os
from collections.abc import Iterable

def save_dict_to_hdf5(dic, filename):
    """
    ....
    """
    with h5py.File(filename, 'w') as h5file:
        recursively_save_dict_contents_to_group(h5file, '/', dic)

def recursively_save_dict_contents_to_group(h5file, path, dic):
    """
    ....
    """
    for key, item in dic.items():
        if isinstance(item, (np.ndarray, np.int64, np.float64, str, bytes)):
            h5file[path + key] = item
        elif isinstance(item, dict):
            recursively_save_dict_contents_to_group(h5file, path + key + '/', item)
        else:
            raise ValueError('Cannot save %s type'%type(item))

def load_dict_from_hdf5(filename):
    """
    ....
    """
    with h5py.File(filename, 'r') as h5file:
        return recursively_load_dict_contents_from_group(h5file, '/')

def recursively_load_dict_contents_from_group(h5file, path):
    """
    ....
    """
    ans = {}
    for key, item in h5file[path].items():
        if isinstance(item, h5py._hl.dataset.Dataset):
            ans[key] = item[()]
        elif isinstance(item, h5py._hl.group.Group):
            ans[key] = recursively_load_dict_contents_from_group(h5file, path + key + '/')
    return ans


def load_features_from_hdf5(filename, paths):
    """
    ....
    """
    h5data={}
    with h5py.File(filename, 'r') as h5file:
        for key, value in paths.items():
            
            data=h5file[value][()]
            #if not isinstance(data, Iterable):
                #data=np.array([data]*nshots)
                #print(data)
            
            h5data[key] = data

        
    return h5data

def load_slice_from_hdf5(filename, path, index):
    """
    ....
    """
    h5data = {}
    with h5py.File(filename, 'r') as h5file:
        data= h5file[path][index]
        
    return data


def load_metadata_from_hdf5(filename, path):
    """
    ....
    """
    h5data = {}
    with h5py.File(filename, 'r') as h5file:
        data= h5file[path][()]
        
    return data
    #return h5data


def check_integrity(filename):
    """
    ....
    """
    #h5data = {}
    try:
        h5file = h5py.File(filename, 'r')
        h5file.close()
        return True
    except:
        return False
