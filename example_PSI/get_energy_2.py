#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from olaf.parser import Parser
import argparse
from olaf.yamltools import read_yaml
#from olaf.csvtools import list_to_csv
import os
import pickle
import json

parser = argparse.ArgumentParser()

parser.add_argument("analysis_file", help="Analysis file", nargs='+')

args = parser.parse_args()

aa = Parser()



for name in args.analysis_file:


    inname = name.split("_")

    runname = inname[0]
    acqname = inname[1]
    pid = int(inname[2])
    #/sf/maloja/data/p19582/raw/run0256/meta/acq0026.json

    filename = "/sf/maloja/data/p19582/raw/run"+runname + "/meta/acq" + acqname + ".json"
    #print(filename, pid)

    # Opening JSON file
    f = open(filename)
    
    # returns JSON object as 
    # a dictionary
    data = json.load(f)
    
    print("[" + name + " , " + str(data["scan_info"]["scan_values"][0])+ " , " + str(data["scan_info"]["scan_readbacks"][0])+ " ],")

# Iterating through the json
# list
#for i in data['emp_details']:
    #print(i)
  
# Closing file
f.close()


#origdata = []
#origdata = aa.load(filename)

#print(len(origdata))

#proplist = aa.get_properties(origdata)

#data = aa.filter(origdata, "pid","==", pid)



#print(data)

#print("Energy ", data[0]["photon_energy"])


#patterns=  aa.get_data(data, "JF15T08V01")


#import matplotlib.pyplot as plt
#import matplotlib

#matplotlib.image.imsave('test.png', pattern)

#for ipatt, pattern in enumerate(patterns):
    
    #locpattern = np.copy(pattern)
    #minval = np.amin(locpattern[locpattern>0])
    #locpattern[locpattern<=0]=minval
    
    #plt.imshow(locpattern, norm=matplotlib.colors.LogNorm(), origin="lower")
    #print(sorted_data[ipatt])
    #plt.show()

#print(patterns.shape)

exit(0)

