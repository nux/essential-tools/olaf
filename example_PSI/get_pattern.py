import numpy as np
from sfdata import SFDataFiles
import argparse
from PIL import Image
import h5py

def load_mask(filename):
    

    img = Image.open(filename).convert('L')
    mask=np.array(img, dtype=float)
    mask=mask/np.amax(mask)
    mask[mask<0.5]=0
    mask[mask>=0.5]=1
    
    return mask


def load_pattern_from_sf(filename, pids):
    """
    ....
    """

    with SFDataFiles(filename) as datafile:
        channel_name = "JF15T08V01"
        subset = datafile[[channel_name]]
        channel  = subset[channel_name]
        origpids = list(channel.datasets.pids[:,0])
        
        
        indexes = np.array([origpids.index(i) for i in pids])

        return channel[indexes]
        

parser = argparse.ArgumentParser()

parser.add_argument('file', help="filename")
parser.add_argument('pid', help="filename")
parser.add_argument('mask', help="mask")



args = parser.parse_args()


filename = args.file
pid = int(args.pid)
maskfilename = args.mask

data = load_pattern_from_sf(filename, [pid])
mask = load_mask(maskfilename)

pattern=data[0]


outname=str(pid)+".h5"

h5file = h5py.File(outname, "w")
h5file.create_dataset("data", data=pattern)
h5file.create_dataset("id", data=pid)
h5file.create_dataset("mask", data=mask)
h5file.close()


import matplotlib.pyplot as plt
plt.imshow(np.log(pattern)*mask)
plt.show()
