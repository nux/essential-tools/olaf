#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from olaf.parser import Parser
import argparse
from olaf.yamltools import read_yaml
#from olaf.csvtools import list_to_csv
import os
import pickle

parser = argparse.ArgumentParser()

parser.add_argument("analysis_file", help="Analysis file", nargs='+')

args = parser.parse_args()

aa = Parser()

origdata = []
for filename in args.analysis_file:
    print("loading", filename)
    origdata+=aa.load(filename)

delays = []


for oo in origdata:
    delays.append(oo["time_delay"])
    
delays = np.unique(delays)

print(delays)

delaydata={}

for d in delays:
    delaydata[str(d)]=[]


for oo in origdata:
    tdelay = oo["time_delay"]
    delaydata[str(tdelay)].append(oo)
    
delayavg = {}

   
   

fig, ax = plt.subplots(1,1,figsize=(8,5))


numbrights = [10,50,200, 1000]

for inb in range(len(numbrights)):
    nb = numbrights[inb]
    
    nbmin = 0
    if inb>0:
        nbmin = numbrights[inb-1]
        
    for dk in delaydata.keys():
        print(dk, len(delaydata[dk]))
        brightest = aa.sort(delaydata[dk] , "intensity")[::-1][nbmin:nb]
        brightvals = [xx["intensity"] for xx in brightest]
        
        
        delayavg[dk] = np.mean(brightvals)

    
    print(delayavg)    


    data = np.array([delayavg[dk] for dk in delayavg.keys()])
    ax.plot(delays, data/np.mean(data) , label=str(nbmin) + " to "+str(nb) + " brightest")
    
    
ax.set_xlabel("Time delay (ps)")
ax.set_ylabel("Average intensity of brightest shots")
ax.legend()
plt.savefig("plot.png", dpi=300)
#plt.show()



#print(delaydata)

#print(len(origdata))

#proplist = aa.get_properties(origdata[0])

#print(*proplist, sep="\n")

#for oo in origdata:
    #oo = aa.sort(oo, "intensity")[::-1]




#sorted_data=aa.sort(origdata, "intensity")[::-1]

#print(aa.get_value(sorted_data, "intensity"))


#data = aa.filter(origdata, ["hit"], True)
#data2 = aa.filter(origdata, "hit", True)

#data = aa.filter(origdata, ["hit"], "==", True)
#data2 = aa.filter(origdata, "hit", "==", True)


#sorted_data=aa.sort(sorted_data[-10:], "radial_extension")

#if len(sorted_data)>10:
    #sorted_data=sorted_data[:10]

#patterns=  aa.get_data(sorted_data, "JF15T08V01")


#import matplotlib.pyplot as plt
#import matplotlib

##matplotlib.image.imsave('test.png', pattern)

#for ipatt, pattern in enumerate(patterns):
    
    #locpattern = np.copy(pattern)
    #minval = np.amin(locpattern[locpattern>0])
    #locpattern[locpattern<=0]=minval
    
    #plt.imshow(locpattern, norm=matplotlib.colors.LogNorm(), origin="lower")
    #print(sorted_data[ipatt])
    #plt.show()

##print(patterns.shape)

#exit(0)

#patterns=[]
#for item in sorted_data:
    #patterns.append( aa.get_data(item, "vmi/andor"))

#tof = aa.get_from_h5(sorted_data, "digitizer/channel1")[-1]


#print(aa.get_value(sorted_data, ["ROI", "left", "size"]))


#print(pattern.shape)

##list_to_csv(os.path.join(settings["workdir"], "test.csv"), sorted_data, 'w')
#saved_data=origdata[:20]
#aa.save(saved_data, "test_data/test_filter.csv", mode='w')



#csv_data=aa.load("test_data/test_filter.csv")
#print()

#for i in range(len(saved_data)):
    #print()
    #print(saved_data[i])
    #print(csv_data[i])

##aa.save(sorted_data, "test_data/test_filter.json", mode='w')

#import matplotlib.pyplot as plt
#import matplotlib

##matplotlib.image.imsave('test.png', pattern)

#for ipatt, pattern in enumerate(patterns):
    
    #vmax = np.mean(pattern)
    #plt.imshow(pattern, norm=matplotlib.colors.LogNorm(), vmax=vmax, origin="lower")
    #print(sorted_data[ipatt])
    #plt.show()

#plt.plot(tof)
##plt.yscale("log")
#plt.show()


#print(proplist)


#print(props)

##for p in props:
    ##print(list(p.keys()))
    ##print(p['intensity'])


#x_axis=[p["id"] for p in props]
#y_axis=[p["intensity"] for p in props]

#plt.plot(x_axis, y_axis, '*')
#plt.show()

