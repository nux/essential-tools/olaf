'''
All the functions listed here are executed once per ROI, for every shot, and they have access only to the data relative to the shot.
The function must have the following form:


FUNCTION DEFINITION
-------------------

def FUNCION_NAME(settings, experiment_data, mask, properties)

- settings: a dictionary containing all the settings loaded from the configuration file (yaml format) and parsed as nested dictionaries
- experiment_data: contains the data loaded from the h5 file, following the scheme indicated in the "h5" section of the configuration file.
- mask: the binary mask that defines the ROI
- properties: previously computed properties, to allow conditional execution of the function basing for the previous analysis results

For example, if the diffraction patterns are in the dataset "vmi/andor" in the h5 file, and the configuration file contains the following:

h5:
    pattern: "vmi/andor"

then, the pattern relative to the shot is stored in experiment_data["pattern"].
Moreover, it's previously computed property "markername" can be accessed via properties["markername"]

FUNCTION RETURN
---------------

1) The function MUST return a dictionary or nothing. Each key of a dictionary defines a marker name in the analysis file. The dictionary can be empty, or no return value is given, which means that no marker is added to the analysis (useful, for example, if the function is used for data preparation)
2) The returned dictionary can contain multiple keys. The value of each key must be a single value 
    
    
IMPORTANT NOTES
---------------

* The functions are called in the order they are written in this file. 

* All the functions are executed. However, it is possible to create local functions, that are not directly called during the analysis, by giving them a name that starts with "_". For example, the following function:

def _get_sum(a,b):
    return a+b
    
is ok, and can be used by the other functions in this file. Instead, the function

def get_sum(a,b):
    return a+b
    
will be executed during the analysis.

* the types of the markers must be native python types, which means int, float and string

'''


import numpy as np
from olaf.booster import get_polar_image
from olaf.booster import sphere as mie_sphere

import matplotlib.pyplot as plt
import scipy.optimize



 # Parameters:
 






def get_intensity(settings, experiment_data, mask, properties):
    '''
    Get the intensity of this ROI
    '''

    return {"intensity": np.sum(experiment_data["pattern"][mask>0.5], dtype=float)}

def _autocorr(x):
    result = np.correlate(x, x, mode='same')
    return result[result.size//2:]



def get_polar(settings, experiment_data, mask, properties):
    "Get a polar representation of the diffraction pattern"
    if properties["hit"]==1:
        
        polarshape=[256,512]
                
        pattern=np.copy(experiment_data["pattern"]).astype(float)
        pattern[pattern<0]=0
        pattern[mask<0.5]=-1
        
        experiment_data["polar_pattern"] = np.array(get_polar_image(pattern, settings["dtheta"], settings["center"], angles=settings["polar_angle"], size=polarshape) )
        #plt.imshow(experiment_data["polar_pattern"])
        #plt.show()
    


def get_asphericity(settings, experiment_data, mask, properties):
    '''
    Get an estimate of how much (non)spherical is the diffraction pattern
    '''
    if properties["hit"]==1:

        polarshape=experiment_data["polar_pattern"].shape

        maskedpolar=np.ma.masked_array(experiment_data["polar_pattern"] , experiment_data["polar_pattern"] <0)
        
        
        nprofiles=128
        valnprofiles=4
        profsize=experiment_data["polar_pattern"].shape[0]//nprofiles
        
        averages=[]
        stds=[]
        #avg=np.ma.average(maskedpolar, axis=1)

        for iprof in range(nprofiles):
            
            ang_prof=np.ma.average(maskedpolar[iprof*profsize:(iprof+1)*profsize, :], axis=0).filled(-1)
            masksize=len(ang_prof>=0)
            if masksize>3:
                
                
                aver = np.mean(ang_prof[ang_prof>=0])
                #print(aver)
                std = np.std(ang_prof[ang_prof>=0])
                #print(std)
                if (not np.isnan(aver)) and (not np.isnan(std)):
                    averages.append(aver)
                    stds.append(std)
            
        
        
        averages=np.array(averages)
        stds=np.array(stds)
        
        #print(averages)
        #print(stds)
        
        valinds = np.argsort(averages)[::-1][:valnprofiles]
        
        asphericity = np.average(stds[valinds]/averages[valinds])
        return {"asphericity" : asphericity}
        
    return {"asphericity" : None}  


def get_polar_ft(settings, experiment_data, mask, properties):
    
    temppolar = np.copy(experiment_data["polar_pattern"])
    temppolar[temppolar<0]=0
    ft = np.fft.fft2(temppolar)
    experiment_data["polar_pattern_ft"]=ft
    
    
    #xproj=np.sum(np.abs(ft), axis=1)
    #yproj=np.sum(np.abs(ft), axis=0)
    #xproj=xproj/xproj[0]
    #yproj=yproj/yproj[0]
    
    
    #fig, ax = plt.subplots(2,2, figsize=(16,16))
    #ax=ax.flatten()
    
    #ax[0].imshow(np.log(np.abs(ft)))
    #ax[1].imshow(np.log(temppolar))
    #ax[2].imshow(mask)
    #tempdata= np.copy(experiment_data["pattern"])
    #vmin = np.amin(tempdata[tempdata>0])
    #tempdata[tempdata<vmin]=vmin
    
    #ax[3].imshow(mask*np.log(tempdata))

    ##ax[2].plot(xproj)
    ##ax[3].plot(yproj)
    
    #plt.show()




def get_autocorr_size(settings, experiment_data, mask, properties):
    
    
    xproj=np.sum(np.abs(experiment_data["polar_pattern_ft"]), axis=1)
    sorted_proj = np.sort(xproj)
    background = np.mean(sorted_proj[:len(sorted_proj)//10])
    xproj = xproj-background
    xproj = xproj/xproj[0]
    xproj[xproj<0]=0
    
    pos=np.linspace(settings["polar_angle"][0],settings["polar_angle"][1] , num=len(xproj)//2, endpoint=False)
    
    meanpos = np.sum(pos*xproj[:len(xproj)//2])/np.sum(xproj[:len(xproj)//2])
    
    return {"radial_extension" : meanpos}



def get_autocorr_angular(settings, experiment_data, mask, properties):
    
    
    yproj=np.sum(np.abs(experiment_data["polar_pattern_ft"]), axis=0)
    sorted_proj = np.sort(yproj)
    background = np.mean(sorted_proj[:len(sorted_proj)//10])
    yproj = yproj-background
    yproj = yproj/yproj[0]
    yproj[yproj<0]=0
    
    pos=np.linspace(0,180 , num=len(yproj)//2, endpoint=False)
    
    meanpos = np.sum(pos*yproj[:len(yproj)//2])/np.sum(yproj[:len(yproj)//2])
    
    return {"angular_correlation" : meanpos}


#def get_asphericity(settings, experiment_data, mask, properties):
    #'''
    #Get an estimate of how much (non)spherical is the diffraction pattern
    #'''
    #if properties["hit"]==1:

        #polarshape=experiment_data["polar_pattern"].shape

        #maskedpolar=np.ma.masked_array(experiment_data["polar_pattern"] , experiment_data["polar_pattern"] <0)
        
        
        #nprofiles=16
        #profsize=experiment_data["polar_pattern"].shape[1]//nprofiles
        
        #profiles=[]
        #radial_profile_avg=np.ma.average(maskedpolar, axis=1)

        #for iprof in range(nprofiles):
            
            #radial_profile=np.ma.average(maskedpolar[:, iprof*profsize:(iprof+1)*profsize], axis=1)
            #radial_profile=radial_profile[1:]-radial_profile[:-1]
            #mask_prof=np.logical_not(np.ma.getmask(radial_profile))
            #std=np.std(radial_profile)
            #radial_profile=np.clip(radial_profile, -1.5*std, 1.5*std)
            #radial_profile_smooth=radial_profile.filled(0)
            #radial_profile_smooth=np.convolve(radial_profile_smooth, np.ones([5]), mode="same")
            #radial_profile[mask_prof]=radial_profile_smooth[mask_prof]
            #if(np.sum(1-np.ma.getmask(radial_profile))>32):
                #profiles.append(radial_profile)

        
        #corrind=[]
        
        #for iprof in range(len(profiles)-1):
            #for jprof in range(iprof+1,len(profiles)):
                #mask1= np.logical_not(np.ma.getmask(profiles[iprof]))
                #mask2= np.logical_not(np.ma.getmask(profiles[jprof]))
                ##print(mask1)
                #maskmerge= (mask1)*(mask2)
                
                #if np.sum(maskmerge)>16:
                    #result = np.correlate(profiles[iprof][maskmerge], profiles[jprof][maskmerge], mode='same')
                    
                    #sorted_inds=np.argsort(result)[-4]
                    ##ind = sorted_inds(np.amax(sorted_inds-result.shape[0]//2))
                    #corrind.append(np.amin(np.abs(sorted_inds-result.shape[0]//2)))
                    
                    ##plt.plot(result)
                    ##plt.show()
                    ##corrind.append(np.argmax(result)-result.shape[0]//2)
        #corrind=np.array(corrind)
        #corrind=corrind[np.argsort(np.abs(corrind))]
        #asphericity=np.mean(corrind[:int(0.66*len(corrind))])
        #return {"asphericity" : asphericity}
    #else:
        #return {"asphericity" : None}        

#def get_radial_correlation(settings, experiment_data, mask, properties):
    #'''
    #Get an estimate of how much the signal is correlated along the radial direction
    #'''
    #if properties["hit"]==1:

        #polarshape=experiment_data["polar_pattern"].shape

        #maskedpolar=np.ma.masked_array(experiment_data["polar_pattern"] , experiment_data["polar_pattern"] <0)

        #radial_profile=np.ma.average(maskedpolar, axis=1)

        #radial_profile=radial_profile.filled(-1)
        #radial_profile=radial_profile[radial_profile>=0]

        #radial_profile_derivative=radial_profile[1:]-radial_profile[:-1]

        #radial_profile_autocorr=_autocorr(radial_profile_derivative)

        #radial_corr=np.sum(np.abs(radial_profile_autocorr))/radial_profile_autocorr[0]

        ##print(radial_profile)
        ##print(radial_profile.shape)
        ##plt.plot(radial_profile_derivative)
        ##plt.show()
        ##plt.plot(radial_profile_autocorr)
        ##plt.show()   
        
        ##fig, ax=plt.subplots(1,3, figsize=(18,6))
        ##ax[0].plot(radial_profile_autocorr[:40]/radial_profile_autocorr[0])
        ##ax[1].plot(radial_profile_derivative)
        ##ax[2].imshow(maskedpolar)
        ##plt.show()
        ##plt.close()
        
        #return {"rad_autocorr": radial_corr-1.}
    #else:
        #return {"rad_autocorr": None}


#def _exp_function(x, a, b, c):
    #return a * np.exp(-b * x) + c

#def get_radial_slope(settings, experiment_data, mask, properties):
    #'''
    #Get an estimate of the slope of the radial profile. The radial profile is fitted with an exponential function a * np.exp(-b * x) + c, and the fitted b coefficient is returned as "rad_slope" marker
    #'''
    #if properties["hit"]==1:

        #polarshape=experiment_data["polar_pattern"].shape

        #maskedpolar=np.ma.masked_array(experiment_data["polar_pattern"] , experiment_data["polar_pattern"] <0)

        #radial_profile=np.ma.average(maskedpolar, axis=1)
        
        #pixmask=np.logical_not(np.ma.getmask(radial_profile))
        
        #pixcoords=np.arange(0, len(radial_profile), 1)
        #pixcoords=pixcoords[pixmask]
        
        ##radial_profile=radial_profile.filled(-1)
        #radial_profile=radial_profile[pixmask]
        
        #slope0=- (np.log(radial_profile[-1]) - np.log(radial_profile[0]))/(pixcoords[-1]-pixcoords[0])
        
        #p0=[radial_profile[0],slope0,1]
        
        #try:
            #popt, pcov = scipy.optimize.curve_fit(_exp_function, pixcoords, radial_profile, p0=p0)        
        
            ##fig, ax=plt.subplots(1,3, figsize=(18,6))
            ##ax[0].plot(pixcoords, radial_profile)
            ##ax[1].plot(pixcoords, _exp_function(pixcoords, *popt))
            ##ax[2].imshow(maskedpolar)
            ##plt.show()
            ##plt.close()
            
            #return {"rad_slope": popt[1]}
        #except:
            #return {"rad_slope": -1}

    #else:
        #return {"rad_slope": None}




#def get_size(settings, experiment_data, mask, properties):
    #'''
    #Get an estimate of the slope of the radial profile. The radial profile is fitted with an exponential function a * np.exp(-b * x) + c, and the fitted b coefficient is returned as "rad_slope" marker
    #'''
    #if properties["hit"]==1:

        #polarshape=experiment_data["polar_pattern"].shape

        #maskedpolar=np.ma.masked_array(experiment_data["polar_pattern"] , experiment_data["polar_pattern"] <0)

        #radial_profile=np.ma.average(maskedpolar, axis=1)
        #pixmask=np.logical_not(np.ma.getmask(radial_profile))


        #radial_profile_diff=radial_profile[1:]- radial_profile[:-1]
        #pixmask_diff=np.logical_not(np.ma.getmask(radial_profile_diff))
        ##print(len(pixmask))
        ##radial_profile_diff=radial_profile_diff/np.mean(np.abs(radial_profile_diff))
        ##radial_profile_diff=radial_profile_diff[pixmask]
       
       
        #absdiff = np.abs(radial_profile_diff[pixmask_diff])
        #absdiff=np.sort(absdiff)
        #threshold=absdiff[int(absdiff.shape[0]*0.95)]
        ##print(threshold)
        #pixmask_diff[np.abs(radial_profile_diff)>threshold]=False
        ##print(pixmask_diff)
        
        #tgs = np.linspace(np.tan(polar_min_angle/180.*np.pi), np.tan(polar_max_angle/180.*np.pi), radial_profile.shape[0])
        #angles = np.arctan(tgs)/np.pi*180.
        ##print(angles)
        #angles_diff = angles[:-1]
        ##angles = angles[pixmask]
        ##print(len(angles))
        #radial_profile_diff=radial_profile_diff/np.mean(np.abs(radial_profile_diff[pixmask_diff]))
        
        
        #def get_sim_data(X):
            ##sim = np.asarray(mie_sphere(X[0], 0.05,0.05, angles+X[1]))
            
            #sim = np.sin(angles[:-1]/angles[-1]*2.*np.pi*X[0]+X[1])
            
            #return sim
        
        #def target(X):
            #simdiff= get_sim_data(X)
            #corrval = np.sum(simdiff[pixmask_diff]*radial_profile_diff[pixmask_diff])
            #return -corrval
            
        
        ##try:
        #tries = 10
        #errors=[]
        #results=[]
        #sizes=np.linspace(1,20, tries)
        #bounds=[
            #[1,25],
            #[-3,3]
            #]
        #for itry in range(tries):
            ##x0=[ sizes[itry], np.random.uniform(-0.1,0.1), np.random.uniform(0,0.2)]
            #x0=[ sizes[itry], np.random.uniform(-1,1)]

            ##res = scipy.optimize.minimize(target, x0=x0, method="Nelder-Mead", options={'xatol': 1e-8, 'fatol': 1e-8, 'maxfev':200})        
            #res = scipy.optimize.minimize(target, x0=x0, bounds=bounds)#, options={'xatol': 1e-8, 'fatol': 1e-8})        

            #errors.append(target(res.x))
            #results.append(res.x)
            
        #bestres=results[np.argmin(errors)]
        #simdiff= get_sim_data(bestres)
        ##print(bestres)
        ##plt.plot(angles_diff[pixmask_diff], radial_profile_diff[pixmask_diff])
        ##plt.plot(angles_diff[pixmask_diff], simdiff[pixmask_diff])
        ##plt.show()
        #return {"size": bestres[0]}
        ##except:
            ##return {"size": -1}
    #else:
        #return {"size": None}

