
import h5py
import glob
import numpy as np
import matplotlib.pyplot as plt
from multiprocessing import Pool, Queue


def _get_module(filename, index, startind, endind):

    #print(filename)
    with h5py.File(filename, 'r') as h5file:
        is_good = h5file["data/JF15T08V01/is_good_frame"][startind:endind,0].astype(bool)
        pids =  h5file["data/JF15T08V01/pulse_id"][startind:endind,0]
        frameind = h5file["data/JF15T08V01/frame_index"][startind:endind,0]
        goodinds = is_good.nonzero()[0]
        #print(is_good)
        #print(pids)
        #print(frameind)
        data = []
        if index>=0:
            modulesize=512
            moduleoffset=modulesize*index
            data = h5file["data/JF15T08V01/data"][startind+goodinds,moduleoffset:moduleoffset+modulesize, :]

        else:
            data = h5file["data/JF15T08V01/data"][startind+goodinds]

        
        return data, pids[goodinds]
            

def _get_module_parallel(filename, index, pool):
    totsize=0
    with h5py.File(filename, 'r') as h5file:
        totsize=h5file["data/JF15T08V01/pulse_id"].shape[0]
    
    nproc = pool._processes
    
    slicesize = int(np.floor(totsize/(nproc)))
    
    procsargs=[]
    for i in range(nproc):
        startind=i*slicesize
        endind = (i+1)*slicesize
        if endind>totsize:
            endind=totsize
        
        procsargs.append((filename, index, startind, endind))
        print(procsargs[-1])
        
    output = pool.starmap(_get_module, procsargs)
       
    
    print(output)
    
    
                           
    
    
    

#def hitormiss(filename, settings):
    #data, pids, goodinds =_get_module(filename,3, [0,1024])
    #intensities=np.sum(data, axis=(1,2))
    #srtindexes = np.argsort(intensities)
    #sortedint = intensities[srtindexes]
    
    
    #thresh = sortedint[int(len(intensities)*(1.-settings["hit_fraction"]))]
    #filterindex = intensities>thresh
    #totalshots = len(intensities)
    
    
    
    #lowermean = np.mean(sortedint[:int(len(intensities)*settings["miss_fraction"])])
    #std = np.std(intensities-lowermean)
    #threshhitormiss = lowermean+settings["miss_std"]*std
    #hits = len(np.where(intensities>thresh)[0])
    #hitrate = hits/totalshots
    
    #return pids[filterindex], totalshots, np.where(filterindex)[0], hitrate



def hitormiss(infilename, settings, pool):
    files = glob.glob(infilename)
    filename=None
    for file in files:
        if file.find("JF15")>0:
            filename = file
    data, pids =_get_module_parallel(filename,3, pool)
    
    

    
    intensities=np.sum(data, axis=(1,2))
    srtindexes = np.argsort(intensities)
    sortedint = intensities[srtindexes]
    
    #for i in srtindexes[::-1]:
        #plt.imshow(np.log(data[i]))
        #plt.show()
    #thresh = sortedint[int(len(intensities)*(1.-settings["hit_fraction"]))]
    #filterindex = intensities>thresh
    totalshots = len(intensities)
    
    
    
    lowermean = np.mean(sortedint[:int(len(intensities)*settings["miss_fraction"])])
    std = np.std(sortedint[:int(len(intensities)*settings["miss_fraction"])])
    threshhitormiss = lowermean+settings["miss_std"]*std
    filterindex = intensities>threshhitormiss

    hits = len(np.where(filterindex)[0])
    hitrate = hits/totalshots
    
    return pids[filterindex], totalshots, np.where(filterindex)[0], hitrate


#def hitormiss(filename, settings):
    #data, pids, goodinds =_get_module(filename,3, [0,1024])
    #intensities=np.sum(data, axis=(1,2))
    #srtindexes = np.argsort(intensities)
    #sortedint = intensities[srtindexes]
    
    #lowermean = np.mean(sortedint[:int(len(intensities)*settings["miss_fraction"])])
    #std = np.std(intensities-lowermean)

    #thresh = lowermean+settings["miss_std"]*std

    #filterindex = intensities>thresh

    #totalshots = len(intensities)
    #return pids[filterindex], totalshots, np.where(filterindex)[0]

#def hitormiss(filename):
    #data, pids, goodinds =_get_module(filename,3, [0,1024])
    #intensities=np.sum(data, axis=(1,2))
    #srtindexes = np.argsort(intensities)
    #sortedint = intensities[srtindexes]
    
    #lowermean = np.mean(sortedint[:int(len(intensities)*3./4.)])
    #std = np.std(intensities-lowermean)
    
    ##print(lowermean, std)
    
    #thresh = lowermean+2.*std
    ##print(thresh)
    #filterindex = intensities>thresh
    
    ##plt.hist(intensities, bins=100)
    ##plt.show()
    
    ##hitrate = np.sum(filterindex)/len(intensities)
    ##    
    ##plt.hist(intensities[np.logical_not(filterindex)], bins=100)
    ##plt.show()
    
    ##print(pids[filterindex])   
    ##exit(0) 
    #totalshots = len(intensities)
    #return pids[filterindex], totalshots, np.where(filterindex)[0]
