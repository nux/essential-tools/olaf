import os
import h5py
import argparse
import matplotlib
import numpy as np
from sfdata import SFDataFiles

matplotlib.use("agg")
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import seaborn as sns
from PIL import Image

sns.set_style("whitegrid")


def load_mask(filename):
    img = Image.open(filename).convert('L')
    mask = np.array(img, dtype=float)
    mask = mask / np.amax(mask)
    mask[mask < 0.5] = 0
    mask[mask >= 0.5] = 1

    return mask


def hit_threshold(intensities, miss_fraction=.3, miss_std=4.):
    sorted_indexes = np.argsort(intensities)
    brightest_img_idx = sorted_indexes[-1]
    sorted_int = intensities[sorted_indexes]
    total_shots = len(intensities)

    lower_mean = np.mean(sorted_int[:int(len(intensities) * miss_fraction)])
    std = np.std(sorted_int[:int(len(intensities) * miss_fraction)])
    thresh_hit_or_miss = lower_mean + miss_std * std
    filter_index = intensities > thresh_hit_or_miss

    hits = len(np.where(filter_index)[0])
    hit_rate = hits / total_shots

    slightly_too_dim_idx = np.argwhere(intensities == sorted_int[::-1][int(hits) - 1]).squeeze()
    threshold_info = {
        "filter_index": filter_index,
        "hit_rate": hit_rate,
        "thresh_hit_or_miss": thresh_hit_or_miss,
        "brightest_img_idx": brightest_img_idx,
        "slightly_too_dim_idx": slightly_too_dim_idx
    }

    return threshold_info


def load_img_from_sf(filename, pids, mask):
    """
    Grab image data from a full acquisition
    and calculate some stuff on it
    """

    with SFDataFiles(_path) as datafile:
        channel_name = "JF15T08V01"
        subset = datafile[[channel_name]]
        channel = subset[channel_name]
        full_pids = list(channel.pids)
        indexes_full = np.array([full_pids.index(x) for x in full_pids])
        sum_img = 0
        std_approx = 0
        int_images = []
        for i, ii in enumerate(indexes_full):
            iimg = channel[[ii]] * mask
            int_images.append(np.sum(iimg))
            sum_img += iimg
            std_approx += np.sqrt(np.power(iimg - (avg_img / (1 + i)), 2))

        avg_img = avg_img / len(indexes_full)
        std_img = std_approx  / len(indexes_full)

        threshold_infos = hit_threshold(int_images)

        hist_img_int, bins_img_int = np.histogram(int_imgs.reshape([-1]),
                                                  bins="auto", density=True)
        pid_img = channel[np.unique(indexes_pid)] * mask

    img_dict = {
        "avg_img": np.array(avg_img).squeeze(),
        "std_img": np.array(std_img).squeeze(),
        "int_images": np.array(int_images).squeeze(),
        "threshold_infos": threshold_infos,
        "hist_img": np.array(hist_img).squeeze(),
        "bins_img": np.array(bins_img).squeeze(),
        "hist_img_int": np.array(hist_img_int).squeeze(),
        "bins_img_int": np.array(bins_img_int).squeeze(),
        "pid_img": np.array(pid_img).squeeze(),
        "slightly_too_dim_image": np.array(slightly_too_dim_image).squeeze(),
        "brightest_image": np.array(brightest_image).squeeze()
    }

    return img_dict


def load_tof_from_sf(filename, pids):
    """
    Grab TOF data from the Maloja experiment data using SFTools
    """
    with SFDataFiles(filename) as datafile:
        channel_name = "SATES21-GES1:A1_VALUES"
        subset = datafile[[channel_name]]
        channel = subset[channel_name]

        full_pids = list(channel.datasets.pids)
        print(full_pids)
        indexes_full = np.array([full_pids.index(x) for x in full_pids])
        indexes_pid = np.array([full_pids.index(x) for x in pids])
        all_tofs = -np.array(channel[np.unique(indexes_full)]).squeeze()
        avg_tof = np.mean(all_tofs, 0)
        ints_tof = np.sum(all_tofs, -1)
        pid_tof = -np.array(channel[np.unique(indexes_pid)]).squeeze()

    return {"pid_tof": np.array(pid_tof).squeeze(),
            "avg_tof": np.array(avg_tof).squeeze(),
            "ints_tof": np.array(ints_tof).squeeze()}


parser = argparse.ArgumentParser()

parser.add_argument('--pid',
                    help='<Required> The pid',
                    required=True, type=int)
parser.add_argument('--run',
                    help='<Required> The run',
                    required=True, type=int)
parser.add_argument('--acq',
                    help='<Required> The acquisition within the run',
                    required=True, type=int)
parser.add_argument('--mask_file',
                    help='<Required> The path to the mask.tiff in the olaf config folder',
                    required=True, type=str)

base_path = "/sf/maloja/data/p19582/raw"
args = parser.parse_args()

run = int(args.run)
pid = int(args.pid)
acq = int(args.acq)
mask_path = str(args.mask_file)

mask_path = "/das/home/ext-zimmermann_j/p19582/work/analysis/tools/olaf/configs/conf_files_3.0/masks/mask.tiff"
mask = load_mask(mask_path)

_path = os.path.join(base_path, "run{:04.0f}".format(run), "data", "acq{:04.0f}.*.h5".format(acq))
tof_dict = load_tof_from_sf(_path, [pid])
img_dict = load_img_from_sf(_path, [pid], mask)

tof_time_delta = 2.5e-10
tof_x_axis = np.linspace(tof_time_delta,
                         len(tof_dict["pid_tof"]) * tof_time_delta,
                         int(len(tof_dict["pid_tof"])))

# Figure 1: Crazy px
plt.close("all")
context = plt.rc_context({"font.size": 32})
with context:
    f1, ax1 = plt.subplots(2, 2, figsize=(10, 10))
    # The Picture of interest
    lbl_args = (img_dict["pid_img"].sum(), img_dict["threshold_infos"]["thresh_hit_or_miss"],
                tof_dict["pid_tof"].sum(), 3.1e6)
    ax1[0, 0].imshow(img_dict["pid_img"],
                     vmin=0.5, vmax=10,
                     label="Image integral: {:01.0e}\n"
                           "Image hit-threshold{:01.0e}\n"
                           "TOF integral: {:01.0e}\n"
                           "Image hit-threshold{:01.0e}".format(*lbl_args))
    ttl_args = (pid, run, acq)
    ax1[0, 0].set_title("PID: {}, Run: {:01.0f}, Acq: {:01.0f}".format(*ttl_args))
    ax1[0, 0].axis(False)
    ax1[0, 0].grid(False)
    # The TOF Inset
    ax1_inset_1 = ax1[0, 0].inset_axes((.5, .1, .5, .25),
                                       transform=ax1[0, 0].transAxes)
    ax1_inset_1.plot(tof_x_axis, tof_dict["pid_tof"])
    ax1_inset_1.set_xlabel(r"Time of light in $\mu$s")

    # The Histogram
    centroids = (img_dict["bins_img"][1:] + img_dict["bins_img"][:-1]) / 2
    ax[0, 1].hist(centroids, bins=len(img_dict["hist_img"]),
                  weights=img_dict["hist_img"],
                  range=(min(img_dict["bins_img"]),
                         max(img_dict["bins_img"])))
    ax1[0, 1].set_title("Pixel Histogram")

    # The Mean Detector Image
    ax1[1, 0].imshow(img_dict["avg_img"],
                     vmin=0.5, vmax=10)
    ttl_args = (run, acq)
    ax1[1, 0].set_title("Average of Run: {:01.0f}, Acq: {:01.0f}".format(*ttl_args))
    ax1[1, 0].axis(False)
    ax1[1, 0].grid(False)
    # The TOF Inset
    ax1_inset_2 = ax1[1, 0].inset_axes((.5, .1, .5, .25),
                                       transform=ax1[1, 0].transAxes)
    ax1_inset_2.plot(tof_x_axis, tof_dict["avg_tof"])
    ax1_inset_1.set_xlabel(r"Time of light in $\mu$s")

    # The Mean Detector Image
    ax1[1, 1].imshow(img_dict["std_img"],
                     vmin=0.5, vmax=10)
    ttl_args = (run, acq)
    ax1[1, 1].set_title("Std of Run: {:01.0f}, Acq: {:01.0f}".format(*ttl_args))
    ax1[1, 1].axis(False)
    ax1[1, 1].grid(False)

    f1.tight_layout()
    f1.savefig("/das/work/p19/p19582/jzimmermann/crazy_pixels.png", dpi=300)
    plt.close(f)


# Figure 2: Why not hit
plt.close("all")
context = plt.rc_context({"font.size": 32})
with context:
    f1, ax1 = plt.subplots(2, 2, figsize=(10, 10))
    # The Picture of interest
    lbl_args = (img_dict["pid_img"].sum(), img_dict["threshold_infos"]["thresh_hit_or_miss"],
                tof_dict["pid_tof"].sum(), 3.1e6)
    ax1[0, 0].imshow(img_dict["pid_img"],
                     vmin=0.5, vmax=10,
                     label="Image integral: {:01.0e}\n"
                           "Image hit-threshold{:01.0e}\n"
                           "TOF integral: {:01.0e}\n"
                           "Image hit-threshold{:01.0e}".format(*lbl_args))
    ttl_args = (pid, run, acq)
    ax1[0, 0].set_title("PID: {}, Run: {:01.0f}, Acq: {:01.0f}".format(*ttl_args))
    ax1[0, 0].axis(False)
    ax1[0, 0].grid(False)
    # The TOF Inset
    ax1_inset_1 = ax1[0, 0].inset_axes((.5, .1, .5, .25),
                                       transform=ax1[0, 0].transAxes)
    ax1_inset_1.plot(tof_x_axis, tof_dict["pid_tof"])
    ax1_inset_1.set_xlabel(r"Time of light in $\mu$s")

    # The Histogram
    centroids = (img_dict["bins_img_int"][1:] + img_dict["bins_img_int"][:-1]) / 2
    ax[0, 1].hist(centroids, bins=len(img_dict["hist_img_int"]),
                  weights=img_dict["hist_img_int"],
                  range=(min(img_dict["bins_img_int"]),
                         max(img_dict["bins_img_int"])))
    ax1[0, 1].set_title("Intensity Histogram")
    # The slightly too dim image detector Image
    ax1[1, 0].imshow(img_dict["slightly_too_dim_image"],
                     vmin=0.5, vmax=10)
    ttl_args = (run, acq)
    ax1[1, 0].set_title("Just below image threshold")
    ax1[1, 0].axis(False)
    ax1[1, 0].grid(False)

    # # The TOF Inset
    # ax1_inset_2 = ax1[1, 0].inset_axes((.5, .1, .5, .25),
    #                                    transform=ax1[1, 0].transAxes)
    # ax1_inset_2.plot(tof_x_axis, tof_dict["avg_tof"])
    # ax1_inset_1.set_xlabel(r"Time of light in $\mu$s")

    # The Brightest Image
    ax1[1, 1].imshow(img_dict["brightest_image"],
                     vmin=0.5, vmax=10)
    ttl_args = (run, acq)
    ax1[1, 1].set_title("Brightest image")
    ax1[1, 1].axis(False)
    ax1[1, 1].grid(False)

    f1.tight_layout()
    f1.savefig("/das/work/p19/p19582/jzimmermann/hit_or_miss.png", dpi=300)
    plt.close(f)
