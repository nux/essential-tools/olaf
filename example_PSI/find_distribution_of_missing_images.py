import os
import io
import sys
import argparse
import matplotlib
import numpy as np
from sfdata import SFDataFiles

matplotlib.use("agg")
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import seaborn as sns
from glob import glob
from tqdm import tqdm

sns.set_style("whitegrid")


class NoStdStreams(object):
    def __init__(self,stdout = None, stderr = None):
        self.devnull = open(os.devnull,'w')
        self._stdout = stdout or self.devnull or sys.stdout
        self._stderr = stderr or self.devnull or sys.stderr

    def __enter__(self):
        self.old_stdout, self.old_stderr = sys.stdout, sys.stderr
        self.old_stdout.flush(); self.old_stderr.flush()
        sys.stdout, sys.stderr = self._stdout, self._stderr

    def __exit__(self, exc_type, exc_value, traceback):
        self._stdout.flush(); self._stderr.flush()
        sys.stdout = self.old_stdout
        sys.stderr = self.old_stderr
        self.devnull.close()


def get_nr_missing(filename):
    """
    Grab TOF data from the Maloja experiment data using SFTools
    """
    with NoStdStreams():
        with SFDataFiles(filename) as datafile:
             tof_channel_name = "SATES21-GES1:A1_VALUES"
             tof_subset = datafile[[tof_channel_name]]
             tof_channel = tof_subset[tof_channel_name]

             img_channel_name = "JF15T08V01"
             img_subset = datafile[[img_channel_name]]
             img_channel = img_subset[img_channel_name]

             tof_pids = np.array(list(tof_channel.pids)).squeeze()
             img_pids = np.array(list(img_channel.pids)).squeeze()
             # missing_pids = np.setdiff1d(img_pids, tof_pids).squeeze()
             missing_pids = len(img_pids) - len(tof_pids)

    return missing_pids, len(img_pids)


base_path = "/sf/maloja/data/p19582/raw"

start_run = 315
end_run = 320
missing_data = {}
runs = np.arange(start_run, end_run)
for run in tqdm(runs):
    missing_pids = 0
    all_pids = 0
    run_dir = os.path.join(base_path, "run{:04.0f}".format(run), "data")
    sorted_file_list = glob(os.path.join(run_dir, "*.h5"))
    sorted_file_list.sort()
    if sorted_file_list:
        acq_num = int(os.path.basename(sorted_file_list[-1])[3:7])
        for i in tqdm(range(1, 1 + acq_num), leave=False):
            _path = os.path.join(run_dir, "acq{:04.0f}.*.h5".format(i))
            m, a = get_nr_missing(_path)
            missing_pids += m
            all_pids += a
        if all_pids > 0:
            print_args = (run, missing_pids, all_pids, missing_pids/all_pids)
        else:
            print_args = (run, missing_pids, all_pids, 0)
        print("\nRun {:04.0f}\nNr. of missing PIDs: {:01.1f}\nTotal number of PIDs: {} -> {:02.02%}\n".format(*print_args))
        missing_data.update({"{:04.0f}".format(run): print_args[1:]})

dict_values = np.array(list(missing_data.values()))
missing_pids = dict_values[:, 0]
all_pids = dict_values[:, 1]
percentage_missing_pids = dict_values[:, 2] * 100

plt.close("all")
f, ax = plt.subplots(1, 1, figsize=(24, 6))
xx = np.array(list(missing_data.keys())).astype(int)
lns1 = ax.scatter(xx, all_pids, lw=4, label="Available PIDs", c="tab:blue", s=10)

ax2 = ax.twinx()
lns2 = ax2.scatter(xx, percentage_missing_pids, lw=4, label="% missing PIDs: Median: {:02.02f}%".format(np.median(percentage_missing_pids)), c="tab:green", s=10)

ax.set_xlabel("Runs")
ax.set_ylabel("Nr. of available PIDs", color="tab:blue")
ax2.set_ylabel("% of missing PIDs", color="tab:green")

ax.tick_params(axis='y', labelcolor="tab:blue")
ax2.tick_params(axis='y', labelcolor="tab:green")
ax2.grid(False)

ax.set_xticks(runs[::5])
#ax.set_xticklabels(["{:01.0f}".format(x) for x in runs])


lns = [lns1, lns2]
labs = [l.get_label() for l in lns]
ax.legend(lns, labs, fontsize=16, loc='upper center', bbox_to_anchor=(0.5, 1.2))

sns.despine(top=True, bottom=True, left=True,
          right=True, offset=25, trim=True,
          ax=ax)

f.tight_layout()
f.savefig("/das/work/p19/p19582/jzimmermann/available_{}_{}.png".format(start_run, end_run), dpi=300)
plt.close(f)






