import os
import numpy as np
import matplotlib
matplotlib.use("qt5agg")
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import seaborn as sns
from glob import glob
from fnmatch import fnmatch


def scale(data):
    return (data - np.min(data)) / (np.max(data) - np.min(data))

sns.set_style("white")
cmap = cm.turbo
runs = list(np.arange(317, 320))
run_labels = [str(x) for x in runs]
assert len(runs) == len(run_labels)

proxy_key_1 = "pulse_energy"  # The proxy to use for determining the best shots
proxy_key_2 = "intensity"  # The proxy to use for determining the best shots

meta_path = "/das/home/ext-zimmermann_j/p19582/work/analysis/results/meta"
olaf_version = "3.0"  # The version of olaf to use. It's the "version" variable in the used config file
run_search_string = [os.path.join(meta_path, "analysis_{:04.0f}_*{}.csv".format(x, olaf_version)) for x in runs]
all_meta_files = [glob(x) for x in run_search_string] # all meta files
all_meta_files = [item for sublist in all_meta_files for item in sublist]  # flatten the list of lists
all_meta_files.sort()  # in-place sorting

header_names = list(np.genfromtxt(all_meta_files[0], max_rows=1, delimiter=',', dtype=str))
proxy_1_idx = header_names.index("\"{}\"".format(proxy_key_1))
proxy_2_idx = header_names.index("\"{}\"".format(proxy_key_2))

#"SATFE10-PEPG046-EVR0:CALCI"

proxy_1_vals = {}
proxy_2_vals = {}
proxy_1_flat = []
proxy_2_flat = []
for mf in all_meta_files:
    run_ = os.path.basename(mf)[9:13]  # Get Run
    if run_ in proxy_1_vals:
        proxy_1_old_vals = proxy_1_vals[run_]
    else:
        proxy_1_old_vals = []
    if run_ in proxy_2_vals:
        proxy_2_old_vals = proxy_2_vals[run_]
    else:
        proxy_2_old_vals = []

    new_vals = np.genfromtxt(mf,
                             delimiter=',',
                             dtype=float,
                             usecols=[proxy_1_idx, proxy_2_idx],
                             skip_header=1) # Get new data
    if np.shape(new_vals)[0] == 2:
        new_vals = np.expand_dims(new_vals, 0)
    proxy_1_flat.extend(new_vals[:, 0])
    proxy_2_flat.extend(new_vals[:, 1])
    proxy_1_vals.update({run_: proxy_1_old_vals + list(new_vals[:, 0])})  # Concat old and new data
    proxy_2_vals.update({run_: proxy_2_old_vals + list(new_vals[:, 1])})  # Concat old and new data

p1 = np.array(proxy_1_flat, dtype=np.float64)
p2 = np.array(proxy_2_flat, dtype=np.float64)

idx_p1_nonan = np.argwhere(~np.isnan(p1))
idx_p2_nonan = np.argwhere(~np.isnan(p2))
idx_nonan = np.intersect1d(idx_p1_nonan, idx_p2_nonan)

p1 = p1[idx_nonan]
p2 = p2[idx_nonan]

assert p1.shape == p2.shape
corrs = []
for ii in range(1, 11):
    print("Moving {} against {}".format(proxy_key_1, proxy_key_2))
    val = np.corrcoef(p1[ii:], p2[:-ii])[0, 1]
    print(ii, val)
    corrs.append(val)

for ii in range(1, 11):
    print("Moving {} against {}".format(proxy_key_2, proxy_key_1))
    val = np.corrcoef(p1[:-ii], p2[ii:])[0, 1]
    print(ii, val)
    corrs.append(val)




f, ax = plt.subplots(1, 1, figsize=)

































