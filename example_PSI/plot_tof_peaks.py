import os
import argparse
import matplotlib
import numpy as np
from sfdata import SFDataFiles

matplotlib.use("agg")
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import seaborn as sns

sns.set_style("whitegrid")


# from PIL import Image
# import h5py
#
#
# def load_mask(filename):
#     img = Image.open(filename).convert('L')
#     mask = np.array(img, dtype=float)
#     mask = mask / np.amax(mask)
#     mask[mask < 0.5] = 0
#     mask[mask >= 0.5] = 1
#
#     return mask
#
#
# def load_pattern_from_sf(filename, pids):
#     """
#     ....
#     """
#
#     with SFDataFiles(filename) as datafile:
#         channel_name = "JF15T08V01"
#         subset = datafile[[channel_name]]
#         channel = subset[channel_name]
#         origpids = list(channel.datasets.pids[:, 0])
#
#         indexes = np.array([origpids.index(i) for i in pids])
#
#         return channel[indexes]
#
#
# parser = argparse.ArgumentParser()
#
# parser.add_argument('file', help="filename")
# parser.add_argument('pid', help="filename")
# parser.add_argument('mask', help="mask")
#
# args = parser.parse_args()
#
# filename = args.file
# pid = int(args.pid)
# maskfilename = args.mask
#
# data = load_pattern_from_sf(filename, [pid])
# mask = load_mask(maskfilename)
#
# pattern = data[0]
#
# outname = str(pid) + ".h5"
#
# h5file = h5py.File(outname, "w")
# h5file.create_dataset("data", data=pattern)
# h5file.create_dataset("id", data=pid)
# h5file.create_dataset("mask", data=mask)
# h5file.close()
#
# import matplotlib.pyplot as plt
#
# plt.imshow(np.log(pattern) * mask)
# plt.show()

def load_tof_from_sf(filename, pids, avg):
    """
    Grab TOF data from the Maloja experiment data using SFTools
    """
    with SFDataFiles(filename) as datafile:

        channel_name = "SATES21-GES1:A1_VALUES"
        subset = datafile[[channel_name]]
        channel = subset[channel_name]

        origpids = list(channel.datasets.pids)
        if avg:
            indexes = np.array([origpids.index(x) for x in origpids])
            return np.mean(channel[np.unique(indexes)], 0)
        else:
            indexes = np.array([origpids.index(x) for x in pids])
            return channel[np.unique(indexes)]


parser = argparse.ArgumentParser()

parser.add_argument('--pids', nargs='+',
                    help='The pids',
                    required=False)
parser.add_argument('--runs', nargs='+',
                    help='<Required> The runs',
                    required=True)
parser.add_argument('--acqs', nargs='+',
                    help='<Required> The acquisitions within the runs',
                    required=True)
parser.add_argument('--avg', type=str,
                    choices=("True", "true", "False", "false"),
                    default="True")

# Use like:
# python plot_tof_peaks.py --pids 1234 2345 3456 4567 --runs 123 123 2 55 --acqs 1 5 1 10
# Could also be used like: 
# python plot_tof_peaks.py --runs 35 --acqs all
# python plot_tof_peaks.py --runs 35, 44 --acqs all
# python plot_tof_peaks.py --runs 35 --acqs 14 15 16 35

base_path = "/sf/maloja/data/p19582/raw"
args = parser.parse_args()

runs = [int(x) for x in args.runs]
if args.pids:
    pids = [int(x) for x in args.pids]
else:
    pids = None
# acqs = [int(x) for x in args.acqs]
avg = args.avg.lower() == "true"

from os.path import isfile, join

files, run_list = [], []
if len(args.acqs) == 1 and args.acqs[0] == "all":
    for run in runs:
        rundir = base_path + "/run" + f'{run:04}' + "/data/"
        files_ = [int(f[3:7]) for f in os.listdir(rundir) if isfile(join(rundir, f))]
        files_ = list(set(files_))
        files.extend(files_)
        run_list.extend([run] * len(files_))
elif len(runs) == len(args.acqs):
    run_list = runs
    files = [int(x) for x in args.acqs]
elif len(runs) == 1:
    files = [int(x) for x in args.acqs]
    run_list = [runs[0]] * len(files)
else:
    raise NotImplementedError("What are these arguments boi!")
runs = run_list
acqs = files

print(np.shape(runs))
print(np.shape(acqs))

filenames = []
tof_data = []
if pids is None:
    for r, a in zip(runs, acqs):
        _path = os.path.join(base_path, "run{:04.0f}".format(r), "data", "acq{:04.0f}.*.h5".format(a))
        print("Loading " + _path)
        tof_data.append(np.squeeze(load_tof_from_sf(_path, [], avg)))
else:
    for r, a, p in zip(runs, acqs, pids):
        _path = os.path.join(base_path, "run{:04.0f}".format(r), "data", "acq{:04.0f}.*.h5".format(a))
        tof_data.append(np.squeeze(load_tof_from_sf(_path, [p], avg)))

data = -np.array(tof_data)
light_peak_reference = 2205
light_peak_here = 624

lightpeak_offset = light_peak_here - light_peak_reference

charge_state_limits_xenon = {
    "xe2": (13040, 13630),
    "xe3": (10820, 11280),
    "xe4": (9420, 9870),
    "xe5": (8480, 8880),
    "xe6": (7830, 8150),
    "xe7": (7310, 7590),
    "xe8": (6865, 7083),
    "xe9": (6565, 6787),
    "xe11/12": (5741, 6206),
    "xe14/15": (5206, 5520),
    "xeHigh": (4761, 5011),
    "xeHighest": (4426, 4670)
}

charge_state_limits_argon = {
    "ar2": (7630, 7770),
    "ar3": (6320, 6450),
    "ar4": (5555, 5700),
    "ar5": (5050, 5150),
    "ar6": (4600, 4770),
    "ar7": (4340, 4480)
}

for key, val in charge_state_limits_xenon.items():
    charge_state_limits_xenon[key] = tuple([x + lightpeak_offset for x in val])
for key, val in charge_state_limits_argon.items():
    charge_state_limits_argon[key] = tuple([x + lightpeak_offset for x in val])

x_ = np.arange(np.shape(data)[-1])
dist_ = np.max(data) * 0.75
pes = np.arange(655, 761, 3)

print(np.shape(pes))

colors_tofs = cm.turbo(np.linspace(0, 1, len(data)))

colors_cs = cm.turbo(np.linspace(0, 1, len(charge_state_limits_xenon.keys())))
plt.close("all")
context = plt.rc_context({"font.size": 32})
with context:
    f, ax = plt.subplots(2, 1, figsize=(30, 30), gridspec_kw={'height_ratios': [1, 3]})
    tof_integrals = {}
    for i, tof in enumerate(data):
        for ii, (k, v) in enumerate(charge_state_limits_xenon.items()):
            if k in tof_integrals:
                prev_vals = tof_integrals[k]
            else:
                prev_vals = []
            new_vals = [np.sum(tof[v[0]:v[1]])]

            tof_integrals.update({k: prev_vals + new_vals})

        ax[1].plot(x_, 1 / dist_ * (tof) + pes[i], lw=2, c=colors_tofs[i])  # , label="{:03.0f}eV".format(pes[i]))

    for i, (k, v) in enumerate(tof_integrals.items()):
        ax[0].scatter(pes, v, label=k, s=200, color=colors_cs[i])
        ax[0].plot(pes, v, color=colors_cs[i], lw=5)
        ax[1].fill_between(charge_state_limits_xenon[k],
                           [pes[0]], [1 / dist_ * np.max(data[-1]) + pes[-1]],
                           color=colors_cs[i], alpha=.125)

    ax[0].legend(loc=2, ncol=2)
    # ax[1].legend(loc=1)
    # ax[0].set_xticks(range(len(data)))
    # ax[0].set_xticklabels(["{:01.0f}".format(x) for x in pes])

    # ax[1].grid(False)

    # sns.despine(top=True, bottom=True, left=True,
    #            right=True, offset=25, trim=True,
    #            ax=ax[1])

    ax[0].set_yticks([])
    ax[0].set_yticklabels([])
    # ax[0].grid(False)
    # sns.despine(top=True, bottom=True, left=True,
    #            right=True, offset=25, trim=True,
    #            ax=ax[0])

    # for a in ax:
    # a.set_yticks([])
    # a.set_yticklabels([])
    # a.grid(False)
    # sns.despine(top=True, bottom=True, left=True,
    #           right=True, offset=25, trim=True,
    #           ax=a)
    f.tight_layout()
    f.savefig("/das/work/p19/p19582/jzimmermann/xenon.png", dpi=300)
    plt.close(f)

colors_cs = cm.turbo(np.linspace(0, 1, len(charge_state_limits_argon.keys())))
plt.close("all")
context = plt.rc_context({"font.size": 32})
with context:
    f, ax = plt.subplots(2, 1, figsize=(30, 30), gridspec_kw={'height_ratios': [1, 3]})
    tof_integrals = {}
    for i, tof in enumerate(data):
        for ii, (k, v) in enumerate(charge_state_limits_argon.items()):
            if k in tof_integrals:
                prev_vals = tof_integrals[k]
            else:
                prev_vals = []
            new_vals = [np.sum(tof[v[0]:v[1]])]

            tof_integrals.update({k: prev_vals + new_vals})

        ax[1].plot(x_, 1 / dist_ * (tof) + pes[i], lw=2, c=colors_tofs[i])  # , label="{:03.0f}eV".format(pes[i]))

    for i, (k, v) in enumerate(tof_integrals.items()):
        ax[0].scatter(pes, v, label=k, s=200, color=colors_cs[i])
        ax[0].plot(pes, v, color=colors_cs[i], lw=5)
        ax[1].fill_between(charge_state_limits_argon[k],
                           [pes[0]], [1 / dist_ * np.max(data[-1]) + pes[-1]],
                           color=colors_cs[i], alpha=.125)

    ax[0].legend(loc=2, ncol=2)
    # ax[1].legend(loc=1)
    # ax[0].set_xticks(range(len(data)))
    # ax[0].set_xticklabels(["{:01.0f}".format(x) for x in pes])

    # ax[1].grid(False)

    # sns.despine(top=True, bottom=True, left=True,
    #            right=True, offset=25, trim=True,
    #            ax=ax[1])

    ax[0].set_yticks([])
    ax[0].set_yticklabels([])
    # ax[0].grid(False)
    # sns.despine(top=True, bottom=True, left=True,
    #            right=True, offset=25, trim=True,
    #            ax=ax[0])

    # for a in ax:
    # a.set_yticks([])
    # a.set_yticklabels([])
    # a.grid(False)
    # sns.despine(top=True, bottom=True, left=True,
    #           right=True, offset=25, trim=True,
    #           ax=a)
    f.tight_layout()
    f.savefig("/das/work/p19/p19582/jzimmermann/argon.png", dpi=300)
    plt.close(f)
