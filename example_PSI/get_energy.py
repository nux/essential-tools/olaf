#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from olaf.parser import Parser
import argparse
from olaf.yamltools import read_yaml
#from olaf.csvtools import list_to_csv
import os
import pickle

parser = argparse.ArgumentParser()

parser.add_argument("analysis_file", help="Analysis file", nargs='+')
parser.add_argument('-p', '--pid', help='pid', type=int)

args = parser.parse_args()

aa = Parser()
pid = args.pid
origdata = []
for filename in args.analysis_file:
    print("loading", filename)
    origdata += aa.load(filename)

print(len(origdata))

proplist = aa.get_properties(origdata)

data = aa.filter(origdata, "pid","==", pid)



print(data)

print("Energy ", data[0]["photon_energy"])


patterns=  aa.get_data(data, "JF15T08V01")


import matplotlib.pyplot as plt
import matplotlib

#matplotlib.image.imsave('test.png', pattern)

#for ipatt, pattern in enumerate(patterns):
    
    #locpattern = np.copy(pattern)
    #minval = np.amin(locpattern[locpattern>0])
    #locpattern[locpattern<=0]=minval
    
    #plt.imshow(locpattern, norm=matplotlib.colors.LogNorm(), origin="lower")
    #print(sorted_data[ipatt])
    #plt.show()

#print(patterns.shape)

exit(0)

