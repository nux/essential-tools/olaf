import numpy as np
from sfdata import SFDataFiles
import argparse
from PIL import Image
import h5py
import glob
from olaf.parser import Parser
from spring.utils.setup_data import setup_data
import numbers

import matplotlib.pyplot as plt
import matplotlib
    
def load_mask(filename):
    

    img = Image.open(filename).convert('L')
    mask=np.array(img, dtype=float)
    mask=mask/np.amax(mask)
    mask[mask<0.5]=0
    mask[mask>=0.5]=1
    
    return mask


def load_pattern_from_sf(filename, pids):
    """
    ....
    """

    with SFDataFiles(filename) as datafile:
        channel_name = "JF15T08V01"
        subset = datafile[[channel_name]]
        channel  = subset[channel_name]
        origpids = list(channel.datasets.pids[:,0])
        
        
        indexes = np.array([origpids.index(i) for i in pids])

        return channel[indexes]
        

parser = argparse.ArgumentParser()

parser.add_argument('runs', help="runnumber", type=int, nargs='+')
#parser.add_argument('acq', help="acqnumber", type=int)

#parser.add_argument('pid', help="pid", type=int)
#parser.add_argument('mask', help="mask")

dim=512
fraction=0.1


args = parser.parse_args()

runs = args.runs

maskfilename = "/sf/maloja/data/p19582/work/analysis/tools/olaf/configs/conf_files_3.0/masks/mask.tiff"
print("Saving runs", runs)


mask = load_mask(maskfilename)

for run in runs:
    print("Working on Run", run)
    
    analysispath = "/sf/maloja/data/p19582/work/analysis/results/meta/analysis_{:04d}".format(run)+"*_3.0.csv"
    outrun = []
    outacq = []
    outpatterns = []
    outpids = []
    outmask=[]

    outmetadata = []
    
    analysisfiles = glob.glob(analysispath)

    #print(analysisfiles)

    for filename in analysisfiles:
        aa = Parser()
        origdata = []
        print("loading", filename)
        origdata += aa.load(filename)

        #print(len(origdata))

        proplist = aa.get_properties(origdata)

        #print(*proplist, sep="\n")



        sorted_data=aa.sort(origdata, "num_pix_above_noise")[::-1]

        #print(aa.get_value(sorted_data, "num_pix_above_noise"))


        
        #print(len(sorted_data))
        sorted_data=sorted_data[:int(fraction*len(sorted_data))]


        pids = [int(dd["pid"]) for dd in sorted_data]
        locrun = [int(dd["run"]) for dd in sorted_data]
        locacq = [int(dd["acq"]) for dd in sorted_data]
        
        outrun=outrun+locrun
        outacq=outacq+locacq
        outpids=outpids+pids
        
        outmetadata = outmetadata + sorted_data
        
        #print(pids)
        locpatterns= load_pattern_from_sf(sorted_data[0]["filename"], np.array(pids))
        
        for i in range(locpatterns.shape[0]):

        
            data, outmask = setup_data(locpatterns[i], mask,
                                            center= [ 1156, 1107],
                                            cutsize= 2048, 
                                            newsize=dim);
            outpatterns.append(np.copy(data))
            
            
            #locpattern = np.copy(data*outmask)
            #minval = np.amin(locpattern[locpattern>0])
            #locpattern[locpattern<=0]=minval
            
            #plt.imshow(locpattern, norm=matplotlib.colors.LogNorm(), origin="lower")
            #plt.show()

        #matplotlib.image.imsave('test.png', pattern)

        #for ipatt, pattern in enumerate(locpatterns):
            
            #locpattern = np.copy(pattern)
            #minval = np.amin(locpattern[locpattern>0])
            #locpattern[locpattern<=0]=minval
            
            #plt.imshow(locpattern, norm=matplotlib.colors.LogNorm(), origin="lower")
            #print(sorted_data[ipatt])
            #plt.show()

    outpids = np.array(outpids)
    outrun = np.array(outrun)
    outacq = np.array(outacq)
    outpatterns = np.array(outpatterns, dtype=np.float32)

    print(outpids.shape, outrun.shape,outacq.shape,outpatterns.shape)


    outfilename = "Run{:04d}.h5".format(run)

    h5file = h5py.File(outfilename, "w")
    h5file.create_dataset("data", data=outpatterns)
    h5file.create_dataset("id", data=outpids)
    h5file.create_dataset("mask", data=mask)
    h5file.create_dataset("run", data=outrun)
    h5file.create_dataset("acq", data=outacq)
    
    for key in outmetadata[0].keys():
        metadata = np.array([dd[key] for dd in outmetadata])
        if isinstance(metadata[0], numbers.Number)==False:
            metadata = [str(m) for m in metadata]
            
        #print(key, metadata)
        h5file.create_dataset("metadata/"+key, data=metadata)

    h5file.close()

    print("Output written to", outfilename)


