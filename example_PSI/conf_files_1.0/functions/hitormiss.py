
import h5py
import glob
import numpy as np
import matplotlib.pyplot as plt

def _get_module(infilename, index=-1, interval=None):
    files = glob.glob(infilename)
    filename=None
    for file in files:
        if file.find("JF15")>0:
            filename = file
    #print(filename)
    with h5py.File(filename, 'r') as h5file:
        is_good = h5file["data/JF15T08V01/is_good_frame"][:,0].astype(bool)
        pids =  h5file["data/JF15T08V01/pulse_id"][:,0]
        frameind = h5file["data/JF15T08V01/frame_index"][:,0]
        goodinds = is_good.nonzero()[0]
        #print(is_good)
        #print(pids)
        #print(frameind)
        data = []
        if index>=0:
            modulesize=512
            moduleoffset=modulesize*index
            data = h5file["data/JF15T08V01/data"][goodinds,moduleoffset:moduleoffset+modulesize, interval[0]:interval[1]]

        else:
            data = h5file["data/JF15T08V01/data"][goodinds]

        
        return data, pids[goodinds], goodinds
            

#def hitormiss(filename, settings):
    #data, pids, goodinds =_get_module(filename,3, [0,1024])
    #intensities=np.sum(data, axis=(1,2))
    #srtindexes = np.argsort(intensities)
    #sortedint = intensities[srtindexes]
    
    
    #thresh = sortedint[int(len(intensities)*(1.-settings["hit_fraction"]))]
    #filterindex = intensities>thresh
    #totalshots = len(intensities)
    
    
    
    #lowermean = np.mean(sortedint[:int(len(intensities)*settings["miss_fraction"])])
    #std = np.std(intensities-lowermean)
    #threshhitormiss = lowermean+settings["miss_std"]*std
    #hits = len(np.where(intensities>thresh)[0])
    #hitrate = hits/totalshots
    
    #return pids[filterindex], totalshots, np.where(filterindex)[0], hitrate



def hitormiss(filename, settings):
    data, pids, goodinds =_get_module(filename,-1, [0,1024])
    
    

    
    intensities=np.sum(data, axis=(1,2))
    srtindexes = np.argsort(intensities)
    sortedint = intensities[srtindexes]
    
    #for i in srtindexes[::-1]:
        #plt.imshow(np.log(data[i]))
        #plt.show()
    #thresh = sortedint[int(len(intensities)*(1.-settings["hit_fraction"]))]
    #filterindex = intensities>thresh
    totalshots = len(intensities)
    
    
    
    lowermean = np.mean(sortedint[:int(len(intensities)*settings["miss_fraction"])])
    std = np.std(sortedint[:int(len(intensities)*settings["miss_fraction"])])
    threshhitormiss = lowermean+settings["miss_std"]*std
    filterindex = intensities>threshhitormiss

    hits = len(np.where(filterindex)[0])
    hitrate = hits/totalshots
    
    return pids[filterindex], totalshots, np.where(filterindex)[0], hitrate


#def hitormiss(filename, settings):
    #data, pids, goodinds =_get_module(filename,3, [0,1024])
    #intensities=np.sum(data, axis=(1,2))
    #srtindexes = np.argsort(intensities)
    #sortedint = intensities[srtindexes]
    
    #lowermean = np.mean(sortedint[:int(len(intensities)*settings["miss_fraction"])])
    #std = np.std(intensities-lowermean)

    #thresh = lowermean+settings["miss_std"]*std

    #filterindex = intensities>thresh

    #totalshots = len(intensities)
    #return pids[filterindex], totalshots, np.where(filterindex)[0]

#def hitormiss(filename):
    #data, pids, goodinds =_get_module(filename,3, [0,1024])
    #intensities=np.sum(data, axis=(1,2))
    #srtindexes = np.argsort(intensities)
    #sortedint = intensities[srtindexes]
    
    #lowermean = np.mean(sortedint[:int(len(intensities)*3./4.)])
    #std = np.std(intensities-lowermean)
    
    ##print(lowermean, std)
    
    #thresh = lowermean+2.*std
    ##print(thresh)
    #filterindex = intensities>thresh
    
    ##plt.hist(intensities, bins=100)
    ##plt.show()
    
    ##hitrate = np.sum(filterindex)/len(intensities)
    ##    
    ##plt.hist(intensities[np.logical_not(filterindex)], bins=100)
    ##plt.show()
    
    ##print(pids[filterindex])   
    ##exit(0) 
    #totalshots = len(intensities)
    #return pids[filterindex], totalshots, np.where(filterindex)[0]
