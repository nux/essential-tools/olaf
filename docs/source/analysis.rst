Analysis Functions files
========================

Analysis functions can be defined on three differen *levels*: Dataset, Shot and ROI



Dataset analysis file
----------------------

Its path is defined in the configuration file, under the key:

.. code-block:: yaml 

    dataset_analysis:
        filename: "path/to/the/file.py"

        
An example file can be found in :code:`example/functions/dataset_analysis.py`
        

All the functions defined in this file here are executed on the whole dataset once, and they have access to the whole dataset data.

Functions header
~~~~~~~~~~~~~~~~~~~~

The function must have the following form:

.. code-block:: python 

    def FUNCION_NAME(settings, experiment_data, properties)

- :code:`settings`: a dictionary containing all the settings loaded from the configuration file (yaml format) and parsed as nested dictionaries
- :code:`experiment_data`: a dictionary containing the data loaded from the h5 file, following the scheme indicated in the "h5" section of the configuration file.
- :code:`properties`: previously computed properties, to allow conditional execution of the function basing for the previous analysis results

For example, if the diffraction patterns are in the dataset :code:`vmi/andor` in the h5 file, and the configuration file contains the following:

.. code-block:: yaml

    h5:
        pattern: "vmi/andor"
        ...
        ...
    

then, the patterns are stored in :code:`experiment_data["pattern"]`.
All the data is stored such that the first index indicates the shot index within the dataset.
For example, the 2D pattern of the third shot in the dataset can be addressed here as 

.. code-block:: python 

    experiment_data["pattern"][3]
    
It's previously computed property "markername" can be accessed via 

.. code-block:: python 

    properties[3]["markername"]
    

Functions return value
~~~~~~~~~~~~~~~~~~~~~~

1) The function **must return a dictionary or nothing**. Each key of a dictionary defines a marker name in the analysis file. The dictionary can be empty, or no return value is given, which means that no marker is added to the analysis (useful, for example, if the function is used for data preparation)
2) The returned dictionary *can* contain multiple keys. The value of each key can be either:
    - a single value: in that case, the same property is replicated for each shot in the dataset
    - a list of values, where each item refers to a shot

All the following are valid return statements

.. code-block:: python 

    # Do not return any value
    return {}
    return
    
    # Return a marker "hitrate" with a float 
    # value hitrate_value. In this case, the
    # property is replicated for all the 
    # shots automatically

    return {"hitrate": hitrate_value}
    
    # Return a marker "intensity" with a list of
    # float values called intensity_values. 
    # The function is valid only if 
    # len(intensity_values) == number of shots
    # in the dataset
    return {"intensity": intensity_values}    
    
    # A combination of the two previous examples
    return {"hitrate": hitrate_value,
            "intensity": intensity_values} 

Important notes
~~~~~~~~~~~~~~~~~~~~~~

* The functions are called in the order they are written in this file. 

* All the functions are executed. However, it is possible to create local functions, that are not directly called during the analysis, by giving them a name that starts with "_". For example:

.. code-block:: python 

        # This is ok, and can be used by the
        # other functions in this file.
        def _get_sum(a,b):
            return a+b
        
        # This is NOT ok, because the daemon 
        # will try to execute it and raise and error.
        def get_sum(a,b):
            return a+b
    

* the types of the markers must be native python types, which means **int, float and string**

* These functions are not parallelized, as they act on the whole dataset. Be careful when a new computation is added, as it may hugely impact the execution time of the analysis.














Shot analysis file
----------------------

Its path is defined in the configuration file, under the key:

.. code-block:: yaml 

    shot_analysis:
        filename: "path/to/the/file.py"

        
An example file can be found in :code:`example/functions/shot_analysis.py`
        

All the functions defined in this file here are executed on every shot in the dataset, and they have access only to the data relative to that shot.

Functions header
~~~~~~~~~~~~~~~~~~~~

The function must have the following form:

.. code-block:: python 

    def FUNCION_NAME(settings, experiment_data, properties)

- :code:`settings`: a dictionary containing all the settings loaded from the configuration file (yaml format) and parsed as nested dictionaries
- :code:`experiment_data`: a dictionary containing the data loaded from the h5 file, following the scheme indicated in the "h5" section of the configuration file.
- :code:`properties`: previously computed properties, to allow conditional execution of the function basing for the previous analysis results

For example, if the diffraction patterns are in the dataset :code:`vmi/andor` in the h5 file, and the configuration file contains the following:

.. code-block:: yaml

    h5:
        pattern: "vmi/andor"
        ...
        ...
    

then, the pattern of the shot is stored in :code:`experiment_data["pattern"]`.
For example, the 2D pattern dimension of the shot can be addressed here as 

.. code-block:: python 

    experiment_data["pattern"].shape
    
It's previously computed property "markername" can be accessed via 

.. code-block:: python 

    properties["markername"]
    

Functions return value
~~~~~~~~~~~~~~~~~~~~~~

1) The function **must return a dictionary or nothing**. Each key of a dictionary defines a marker name in the analysis file. The dictionary can be empty, or no return value is given, which means that no marker is added to the analysis (useful, for example, if the function is used for data preparation)
2) The returned dictionary *can* contain multiple keys. The value of each key should be a single value.

All the following are valid return statements

.. code-block:: python 

    # Do not return any value
    return {}
    return
    
    # Return a marker "intensity" with a float 
    # value intensity_value. In this case, the
    # property is replicated for all the 
    # shots automatically

    return {"intensity": intensity_value}
    
    # Return a marker "intensity" with a float 
    # value called intensity_value. 
    return {"intensity": intensity_values}    
    
    # A dictionary with multiple keys
    return {"slope": slope_value,
            "intensity": intensity_value} 

Important notes
~~~~~~~~~~~~~~~~~~~~~~

* The functions are called in the order they are written in the file. 

* All the functions are executed. However, it is possible to create local functions, that are not directly called during the analysis, by giving them a name that starts with "_". For example:

.. code-block:: python 

        # This is ok, and can be used by the
        # other functions in this file.
        def _get_sum(a,b):
            return a+b
        
        # This is NOT ok, because the daemon 
        # will try to execute it and raise and error.
        def get_sum(a,b):
            return a+b
    

* the types of the markers must be native python types, which means **int, float and string**












ROI analysis file
----------------------

Its path is defined in the configuration file, under the key:

.. code-block:: yaml 

    ROI_analysis:
        filename: "path/to/the/file.py"

        
An example file can be found in :code:`example/functions/ROI_analysis.py`
        

All the functions defined in this file here are executed for every ROI on every shot in the dataset, and they have access only to the data relative to that shot.

Functions header
~~~~~~~~~~~~~~~~~~~~

The function must have the following form:

.. code-block:: python 

    def FUNCION_NAME(settings, experiment_data, mask, properties)

- :code:`settings`: a dictionary containing all the settings loaded from the configuration file (yaml format) and parsed as nested dictionaries
- :code:`experiment_data`: a dictionary containing the data loaded from the h5 file, following the scheme indicated in the "h5" section of the configuration file.
- :code:`mask`: the binary mask provided under the "ROI" section of the configuration file
- :code:`properties`: previously computed properties, to allow conditional execution of the function basing for the previous analysis results

All the other features are identical to the "Shot" analysis file


