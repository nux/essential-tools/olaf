The olaf.booster submodule
===========================


The :code:`olaf.booster` submodule provides helper functions to perform a faster analysis.

Here is a list of the availbale functions:


.. automodule:: olaf.booster
    :members:
    :imported-members:
..     :exclude-members: get_center_value, get_radial_profile_centers


