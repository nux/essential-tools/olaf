The OLAF daemon
===============

the :code:`olaf-daemon.py` tool is responsible of performing the analysis of the incoming data.
Roughly speaking, the software, when launched, listens for new data files in a given folder. When new data arrives, the daemon reads the data, performs the analysis, and writes the results in the given analysis file.
Detailed instructions on its use and configuration are given in this section.

.. code-block::

    usage: olaf-daemon.py [-h] [--timing] conf_file

    positional arguments:
    conf_file   Configuration file (.yml)

    optional arguments:
    -h, --help  show this help message and exit
    --timing    print timing information


The configuration file
-----------------------

The configuration file has to be written in  `YAML format <https://en.wikipedia.org/wiki/YAML>`_ 

An example of such a file is in :code:`examples/conf.yml`

The following list describes the settings keywords that must be present in the configuration file:


:workdir: **string**

    Indicates the "root" directory for the analysis.  Any path declared later must be relative to this folder.
    
    .. note:: 
                  
        To use absolute paths, just set :code:`workdir="/"`
        
    .. code-block:: yaml 

        workdir: "/storage/experiment/"
    



        
:datapath: **string**

    Path of the folder where the daemon is linstening for new data

    .. code-block:: yaml 

        datapath: "test_data/Run_190/rawdata"
    
    

:workers: **integer**

    Parallel processes used to perform the analysis. Should not exceed the available CPU threads of the machine
    
    .. code-block:: yaml 

        workers: 4
    
    
    
:files_per_dataset: **integer**

    Shots are analyzed in groups (each group represents a dataset). 
    This is the amount of new files that the daemon is waiting for, before starting the analysis.
    If = 3, and the number of shots per file is 50, then shots are analyzed in groups of 150.
    
    .. code-block:: yaml 

        files_per_dataset: 3
    

    
:dataset_timeout: **float**

    Total time in seconds that the analsys daemon will wait for new files. For example, if files_per_dataset=4, but the amount of new files is less for more than :code:`dataset_timeout` seconds, then the analysis will start anyway with the existing files.
    
    .. code-block:: yaml 

        dataset_timeout: 10

        

        
:analysis_file: **string**

    Filename of the analysis file. For now, two formats are supported (csv and json). 
    The format is automatically recognized by its extension.
    
    .. code-block:: yaml 

        analysis_file: "test_data/test.json"

        
:h5: **section**

    The section :code:`h5` defines which data is loaded from the h5 file, and how it is then addressed within the code. See the section about the analysis functions for more explanations
    
    .. code-block:: yaml
        
        h5:
            # The dataset "bunches" in the 
            # h5 file is read, and mapped 
            # to the key "id"
            id: "bunches"
            
            # The dataset "vmi/andor" in the 
            # h5 file is read, and mapped 
            # to the key "pattern"
            pattern: "vmi/andor"
            
            # The dataset "digitizer/channel1" 
            # in the h5 file is read, and mapped
            #  to the key "tof"
            tof: "digitizer/channel1"
            
            # The dataset "endstation/voltages/NHQ_1_B"
            # in the h5 file is read, and mapped 
            # to the key "MCP_voltage"
            MCP_voltage: "endstation/voltages/NHQ_1_B"
    
     
:ROI: **section**

    List of Region Of Interests of the diffraction pattern. The key name defines how the analysis on that ROI will be tagged in the analysis file. 
    The value indicates the path of the masks defining the ROI. 
    The mask has to be a grayscale image, where the ROI is in white (outside the ROI it is black). 
    For now, only grayscale png format is tested. It can be easily created with GIMP.
    
    .. code-block:: yaml
        
        ROI:
            # Use the mask in "masks/mask_left.png"
            # to define a ROI called "left"
            left: "masks/mask_left.png"
            
            # Use the mask in "masks/mask_right.png"
            # to define a ROI called "right"            
            right: "masks/mask_right.png"
            
            
:dataset_analysis: **section**

    This section defines the analysis functions (and eventual parameters) that can access the whole dataset. 
    See the section about the Analysis files for further info.
    
    .. code-block:: yaml
        
        dataset_analysis: 
            # File path of the source file 
            # containing the functions
            filename: "functions/dataset_analysis.py"
            
            # Eventual additional parameters 
            # required by the functions
            miss_fraction: 0.7
            miss_std: 4.
   
   
   
:shot_analysis: **section**

    This section defines the functions (and eventual parameters) used to perform the analysis on a shot-by-shot basis. 
    See the section about the Analysis files for further info.
    
    .. code-block:: yaml
        
        shot_analysis: 
            # File path of the source file 
            # containing the functions
            filename: "functions/shot_analysis.py"
            
            # Eventual additional parameters 
            # required by the functions


            
:ROI_analysis: **section**

    This section defines the functions (and eventual parameters) used to perform the an analysis on a shot-by-shot basis. 
    See the section about the Analysis files for further info.
    
    .. code-block:: yaml
        
        ROI_analysis: 
            # File path of the source file 
            # containing the functions
            filename: "functions/ROI_analysis.py"
            
            # Eventual additional parameters 
            # required by the functions
            dtheta: 0.055
            pattern_center: [707,520] 



            
            
Output Analysis file format
----------------------------

As previously stated, the analysis is performed shot to shot.
Depending on the filename suffix, the analysis can be written in the following formats:

:JSON: (suffix .json)
    | Each line of the file is a standalone JSON object, whose internal structure can be easily interpreted as a nested dictionary
:csv: (suffix .csv)
    | Comma Separate Values. The first line of the file is a header, containing the labels of the colums. Each line after the first represents the analysis result for a single shot. For a given shots, some markers may have no value. In that case, the corresponding entry is replaced by an empty string :code:`""`





