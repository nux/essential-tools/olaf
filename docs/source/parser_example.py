# Parser Example
# Import the Parser object from the olaf.parser submodule
from olaf.parser import Parser

# Initialize the parser, setting the workdir. 
# Paths to the h5 file in the analysis file are relative to the workdir, 
# and thus the workdir is necessary to retrieve the experimental data stored in the h5
par = Parser("/home/alessandro/gitlab.ethz/olaf/")

# Load the analysis. In this case, do not append the workdir to the provided file path.
analysis = par.load("../test_data/test.json", append=False)

# Get the list of all available properties, and print it. 
# Nested properties are shown as /-separated keywords
proplist = par.get_properties(analysis)
print(*proplist, sep="\n")
# Possible output:
#  path
#  filename
#  index
#  hit
#  ROI/left/intensity
#  ROI/left/asphericity
#  ROI/left/rad_autocorr
#  ROI/left/rad_slope
#  ROI/right/intensity
#  ROI/right/asphericity
#  ROI/right/rad_autocorr
#  ROI/right/rad_slope
#  hitrate
#  id
#  tof_intensity


# Filter the analysis to only hits (when the hit flag is True) 
analysis_hits = par.filter(analysis, "hit", "==", True)

# Print the "intensity" values on the "left" ROI for all the shots in analysis_hits
# The key can be expressed in two different ways:
# Option 1:
intensities = par.get_value(analysis_hits, "ROI/left/intensity")
# Option 2:
intensities = par.get_value(analysis_hits, ["ROI","left","intensity"])

# Retrieve the bunch id
ids = par.get_value(analysis_hits, "id")

# Plot the intensity value on the left ROI against the bunch id
import matplotlib.pyplot as plt
plt.plot(ids, intensities, "*")
plt.show()

# Get the shots that have "ROI/left/intensity" greater than 3.6e7
analysis_rint = par.filter(analysis_hits, "ROI/right/intensity", ">", 3.6e7)


# Among analysis_rint, get the 10 shots with lower "ROI/left/asphericity" value
analysis_rint_lspher = par.sort(analysis_rint, "ROI/left/asphericity")[:10]

# Print their "ROI/left/asphericity" values
print(par.get_value(analysis_rint_lspher, "ROI/left/asphericity"))

# Among them, get the shot with higher "tof_intensity"
single_rint_lspher_hitof = par.sort(analysis_rint_lspher, "tof_intensity")[-1]

# Read the wavelength detected by the spectrometer from the h5 file, and print it. 
# Note the mode="m", which means that the "photon_diagnostics/Spectrometer/Wavelength"
# is a single value (metadata), provided only once in the h5 file, and not shot by shot.
wavelength = par.get_data(single_rint_lspher_hitof, 
                          "photon_diagnostics/Spectrometer/Wavelength", mode="m")
print(wavelength)

# Load the pattern and the TOF trace for the given shot (single_rint_lspher_hitof)
pattern = par.get_data(single_rint_lspher_hitof, "vmi/andor", mode="s")
TOF = par.get_data(single_rint_lspher_hitof, "digitizer/channel1", mode="s")


# Plot the pattern in log scale and the TOF trace
import numpy as np
fig, axs = plt.subplots(1,2, figsize=(12,6))
axs[0].imshow(np.log(pattern))
axs[1].plot(TOF)
plt.show()

# Example End
