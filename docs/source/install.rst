Installation
============
The module is mainly in pure Python language. 
An optional submodule is written in C++, and can be installed separately.


The project repository is hosted on the `ETH Gitlab <https://gitlab.ethz.ch/nux/olaf>`_.
The software building system is based on `setuptools <https://setuptools.readthedocs.io/en/latest/>`_. 

Installing the main OLAF software
---------------------------------

These are the steps for installing the main OLAF software.

First, clone the repository and move inside. The repository is not public, but can be downloaded anyway thanks to an access token.
All the process can be done with the following commands on the terminal (Windows PowerShell may work too)

.. code-block:: none

    $ git clone https://TOKEN_NAME:TOKEN_CODE@gitlab.ethz.ch/nux/olaf.git
    $ cd olaf
    

Then, install the software via pip in the user space with the following command:

.. code-block:: none

    $ python3 -m pip install . --user
    
This operation will install:

    :olaf-daemon.py:
        | The analysis software daemon
    
    :the olaf python module:
        | the olaf Python library. In particular, the :code:`olaf.parser` submodule, useful for parsing the analysis file
        
Installing the olaf-booster module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This module is independent from the olaf software itself, and it is somehow optional.
It contains useful functions, written in C++, to help the user in setting up the analysis.

.. note::

    The installation of olaf-booster requries a C++ compiler installed on your machine. For a pre-compiled version of the submodule, please write me at alcolombo@phys.ethz.ch .

To compile and install the olaf-booster module, move in the olaf-booster subfolder and execute again the pip command:

.. code-block:: none

    $ cd olaf-booster
    $ python3 -m pip install . --user

Now the module can be imported in the following ways:

.. code-block:: python

    import olaf.booster
    ##### or #####
    import olaf_booster


Compiling on Windows
~~~~~~~~~~~~~~~~~~~~

These are the steps that should work (to be read as *worked for me*) for compiling and installing the :code:`olaf-booster` python module on Windows.


1. Download and install C++ compiler. I used Visual Studio 2019 (but anyone from 2015 should be fine). It can be downloaded for free from `here <https://visualstudio.microsoft.com/downloads/>`_ (Community version).
#. Download and install `Anaconda <https://www.anaconda.com/products/individual>`_, a developement platform for Python, that includes also the Python interpreter. 
#. Open the Anaconda Prompt program, and navigate to the :code:`olaf-booster` subfolder.
#. | Within the folder, run the command 
   | :code:`python -m pip install .` 
   | (don't forget the final dot)   

.. note::
    
    If you do not have the administration privileges on Windows, you need to add the :code:`--user` option to the install command, that is:
    
    .. code::
        
        python -m pip install . --user
        
