####################################################
Welcome to the OLAF documentation!
####################################################


Introduction
============

Welcome to the OLAF python module documentation! 

OLAF (OnLine Analysis Framework) is a set of Python tools that aims at easing the online analysis for CDI experiments.

OLAF provides:

    :olaf-daemon:
        | The analysis software
    :olaf.booster: 
        | A submodule that provides a fast implementation of the analysis functions
    :olaf.parser: 
        | A submodule that provides an intuitive way to parse and make simple operations on the analysis produced by the :code:`olaf-daemon`
    


Please, read the documentation carefully and report any issue to alcolombo@phys.ethz.ch .

.. toctree::
    :maxdepth: 4
   
    self 

    install
    daemon
    analysis
    booster
    parser
    
..     quickstart

