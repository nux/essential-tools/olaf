The olaf.parser submodule
===========================

Example
-------

The :code:`olaf.parser` submodule provides helper functions to perform operation on the analyzed data.

.. literalinclude:: parser_example.py
   :language: python
   :linenos:
   :lines: 1-51
   :start-after: Parser Example
   :end-before: Get the shots

.. image:: figures/parser_int.png
    :width: 70% 
    :align: center

    
.. literalinclude:: parser_example.py
   :language: python
   :linenos:
   :start-at: Get the shots
   :end-before: Example End
   
.. image:: figures/parser_pattern.png
    :width: 70% 
    :align: center
    
    
Reference
---------

Here is a list of the availbale functions:


.. autoclass:: olaf.parser.Parser
    :members:
    
