#!/usr/bin/env python

from olaf.dataset import Dataset
import os
import numpy as np
from olaf.yamltools import read_yaml
import time
import sys
import io
import argparse
import time
import copy
import re
import glob
import datetime


def get_dir_size(path):
    total = 0
    with os.scandir(path) as it:
        for entry in it:
            if entry.is_file():
                total += entry.stat().st_size
            elif entry.is_dir():
                total += get_dir_size(entry.path)
    return total

def get_most_recent_time(path):
    lasttime = 0
    with os.scandir(path) as it:
        for entry in it:
            if entry.is_file():
                lasttime = max(entry.stat().st_mtime, lasttime)
            elif entry.is_dir():
                lasttime = max(get_most_recent_time(entry.path), lasttime)
    return lasttime
    
    
def get_files(folder, neededruns):

    files = glob.glob(os.path.join(folder,"r*"))

    runinfo=[]
    
    for filename in files:
        match = re.search(r'r(\d\d\d\d)', filename)
        if match:
            runinfo.append([int(match.group(1)), filename])

    #print(len(runinfo), " runs found")

    if neededruns is not None:
        runinfo = [run for run in runinfo if run[0] in neededruns]
        
    return runinfo





def listen_h5(datapath, blacklist,  neededruns, nfiles=1, timeout = 20):
    first_loop=True
    first_wait=True
    timed_out=False
    t0=time.time()
    validsize=0
    minsafter = 3
    while 1:
        time.sleep(2.)
        #print(datapath)
        #exit(0)
        runinfotemp = get_files(datapath, neededruns)
        #print(runinfo)


        runinfo = []
        for i in range(len(runinfotemp)):
            if not (runinfotemp[i][0] in blacklist):
                runinfo.append(runinfotemp[i])
                

        #print(runinfo)

        valid_list = []
        notready_list = []
        empty_list = []
        for i in range(len(runinfo)):
            
            h5list = list(os.scandir(runinfo[i][1]))    
            
            if len(h5list)==0:
                empty_list.append(runinfo[i])
            else:
                modtime = get_most_recent_time(runinfo[i][1])
                timediff = time.time() - modtime
                #print(runinfo[i], "created", timediff/60, "minutes ago")
                        
                
                if timediff>minsafter*60 and len(h5list):
                    valid_list.append(runinfo[i])
                else:
                    notready_list.append(runinfo[i])

        notreadystring = ""
        for f in notready_list:
            notreadystring=notreadystring+" "+f[1]
        if len(notready_list)>0:
            print("Files ",notreadystring, " created less than "+str(minsafter)+" mins ago. Skipping for now." )
        if len(empty_list)>0:
            print("Runs ",notreadystring, " empty. Skipping for now." )

        if len(valid_list)>=nfiles:
            return valid_list[:nfiles]
        
            #print("New files:", newfiles)

        if len(valid_list)>validsize:
            validsize=len(valid_list)
            first_wait=True
            t0=time.time()
    
        print("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b  ",  end='\r')
        if time.time()-t0>timeout and len(valid_list)>0:
            print(" Timed out!")
            return valid_list, valid_runs
        
        if first_wait:
            if validsize>0:
                print("(Timeout in {:.0f}s) Waiting for {:d}/{:d} files in folder ".format(timeout, nfiles-validsize, nfiles), datapath)
                first_wait=False
            else:
                print("Waiting for files in folder ", datapath)
                print("Press Crtl-C to exit.")
                first_wait=False                


def get_tags(intags):
    
    commasplit=intags.split(",")
    #print(commasplit)
    taglist=[]
    for cc in commasplit:
        
        if "-" in cc:
            rangesplit=cc.split("-")
            longest_string = max(rangesplit, key=len)
            width=len(longest_string)
            
            for i in range(int(rangesplit[0]), int(rangesplit[1])+1):
                #taglist.append(str(i).zfill(width))
                taglist.append(i)

        else:
            taglist.append(int(cc))
    return taglist

def replace_keyword(insettings, keyword,  tag):
    settings=copy.deepcopy(insettings)
    for key in settings.keys():
        if isinstance(settings[key], str):
            settings[key]=settings[key].replace(keyword, tag)
            
    #print(settings)
    return settings


def replace_tag(insettings, tag):
    settings=copy.deepcopy(insettings)
    for key in settings.keys():
        if isinstance(settings[key], str):
            settings[key]=settings[key].replace("TAG", tag)
            
    #print(settings)
    return settings


def wait_folder(folder):
    
    valid=False
    firstloop=True
    while not valid:
        check_folder=os.path.exists(folder)
        if check_folder:
            return
        else:
            if firstloop:
                print("Waiting for folder", folder)
            time.sleep(1)
        firstloop=False

def run_daemon():
    
    parser = argparse.ArgumentParser()

    # parser.add_argument("folders", help="Configuration file (.yml)")
    #parser.add_argument('folders', help="Files or folders to analyze", nargs='+')
    parser.add_argument("--conf", help="configuration file. Overrides the OLAF_CONF env variable")
    parser.add_argument('--timing', dest='print_timing', action='store_const',
                        const=True, default=False,
                        help='print timing information')
    parser.add_argument('--check_pids', dest='check_pids', action='store_const',
                        const=True, default=False,
                        help='print pid mismatch information')
    parser.add_argument('--verbose', dest='verbose', action='store_const',
                        const=True, default=False,
                        help='print internal libraries outputs')
    parser.add_argument("--runs",
                        help="Executes the analysis only on specific run numbers. "
                             "Runs can be a single string, or comma separated strings, "
                             "or integer ranges")
    parser.add_argument('--overwrite', dest='overwrite', action='store_const',
                        const=True, default=False,
                        help='silently overwrite existent analysis files')
    args = parser.parse_args()

    conf_file = args.conf
    if conf_file is None:
        conf_file = os.getenv('OLAF_CONF')
        if conf_file is None:
            print("ERROR: conf file not set. Please, set the environment variable OLAF_CONF or pass it via the --conf option.")
            exit(1)

    settings = read_yaml(conf_file)
    version = settings["version"]
    runfolder = settings["rundir"]

    inruns = args.runs
    
    if inruns is not None:
        runs = get_tags(inruns)
    else:
        runs = None

    #print(settings)
    #print(folders)
    #print(runs)
    


    if settings is not None:

        print("Saving analysis in ", settings["outdir"])

        dat = Dataset("/", settings, workers=settings["workers"], inverbose=args.verbose)

        analyzed_runs = []

        if not args.overwrite:
            existing = dat.check_existing_runs()
            print("The following already analyzed runs will be skipped:")
            for run in existing:
                #print(run)
                print("\tRun {:04d} ".format(run))
            analyzed_runs = analyzed_runs + existing
        
        #exit(0)
       
        try:
            while 1:
                
                newruns = listen_h5(runfolder, analyzed_runs , neededruns= runs, nfiles=1, timeout=settings["dataset_timeout"])
                
                if len(newruns)==0:
                    break
                

                print("Analysing runs:")
                for file in newruns:
                    print("\t", file[0],"("+file[1]+")")
                
                
                #try:
                if 1:
                    dat.load_files(newruns[0][1], newruns[0][0])

                    #if len(dat.shots)>0:
                    print("Analysis running")
                    dat.analyze()
                    print("Saving analysis")                        
                    dat.save()

                    if args.print_timing:
                        dat.print_timing()
                    

                        
                #except:
                    #print("Analysis crashed! Skipping run ...")
                
                analyzed_runs = analyzed_runs+newruns[0]
                
                
        except KeyboardInterrupt:
            print('\nAnalysis interrupted by the user. Goodbye!')
            dat.delete()
            try:
                sys.exit(0)
                
            except SystemExit:
                os._exit(0)   

        
if __name__ == '__main__':
    run_daemon()

