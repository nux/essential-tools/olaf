#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from olaf.parser import Parser
import argparse
from olaf.yamltools import read_yaml
from olaf.csvtools import list_to_csv
import os
import pickle

parser = argparse.ArgumentParser()

parser.add_argument("conf_file", help="Configuration file (.yml)")
args = parser.parse_args()


settings = read_yaml(args.conf_file)

aa = Parser(settings["workdir"])

origdata = aa.load(settings["analysis_file"])

proplist = aa.get_properties(origdata)

print(*proplist, sep="\n")

print(aa.get_value(aa.sort(origdata, ["ROI", "whole", "intensity"]),  ["ROI", "whole", "intensity"]))


#data = aa.filter(origdata, ["hit"], True)
#data2 = aa.filter(origdata, "hit", True)

data = aa.filter(origdata, ["hit"], "==", True)
data2 = aa.filter(origdata, "hit", "==", True)


print(len(data), len(data2))
print(pickle.dumps(data)==pickle.dumps(data2))
print("Pippo")

#print(len(aa.filter(["ROI", "left", "intensity"], [145812656.0, None]).data))


#filtered = aa.filter(data, ["ROI", "left", "intensity"], [0., None])

#print(len(filtered.data))

sorted_data = aa.sort(data, ["ROI", "whole", "intensity"])
sorted_data2 = aa.sort(data, "ROI/whole/intensity")

print(len(sorted_data), len(sorted_data2))
print(pickle.dumps(sorted_data)==pickle.dumps(sorted_data2))
#print(len(sorted_data))

sorted_data=aa.sort(sorted_data[-20:], ["ROI", "left", "size"])




#patterns = aa.get_data(sorted_data, "vmi/andor")

patterns=[]
for item in sorted_data:
    patterns.append( aa.get_data(item, "vmi/andor"))

#tof = aa.get_from_h5(sorted_data, "digitizer/channel1")[-1]


print(aa.get_value(sorted_data, ["ROI", "left", "size"]))


#print(pattern.shape)

##list_to_csv(os.path.join(settings["workdir"], "test.csv"), sorted_data, 'w')
#saved_data=origdata[:20]
#aa.save(saved_data, "test_data/test_filter.csv", mode='w')



#csv_data=aa.load("test_data/test_filter.csv")
#print()

#for i in range(len(saved_data)):
    #print()
    #print(saved_data[i])
    #print(csv_data[i])

#aa.save(sorted_data, "test_data/test_filter.json", mode='w')

import matplotlib.pyplot as plt
import matplotlib

#matplotlib.image.imsave('test.png', pattern)

for ipatt, pattern in enumerate(patterns):

    plt.imshow(pattern, norm=matplotlib.colors.LogNorm())
    print(sorted_data[ipatt])
    plt.show()

#plt.plot(tof)
##plt.yscale("log")
#plt.show()


#print(proplist)


#print(props)

##for p in props:
    ##print(list(p.keys()))
    ##print(p['intensity'])


#x_axis=[p["id"] for p in props]
#y_axis=[p["intensity"] for p in props]

#plt.plot(x_axis, y_axis, '*')
#plt.show()

