
import numpy as np
import matplotlib.pyplot as plt
from olaf.parser import Parser
from olaf.yamltools import read_yaml
from olaf.csvtools import list_to_csv
import os


aa = Parser( "/home/alessandro/gitlab.ethz/olaf/")

data = aa.load("test_data/test_237.json")

proplist = aa.get_properties(data)


sorted_data = aa.sort(data, ["ROI", "whole", "intensity"])
patterns=[]
for item in sorted_data[-5:]:
    patterns.append( aa.get_data(item, "vmi/andor"))


import matplotlib.pyplot as plt
import matplotlib

#matplotlib.image.imsave('test.png', pattern)


average=np.mean(np.array(patterns), axis=0)
#plt.imshow(np.log(average))
#plt.show()
average=np.log(average)
average=np.array(average/np.amax(average)*255, dtype=np.uint8)


from PIL import Image
im = Image.fromarray(average)
im.save("average.png")

#for ipatt, pattern in enumerate(patterns):

    #plt.imshow(pattern, norm=matplotlib.colors.LogNorm())
    #print(sorted_data[ipatt])
    #plt.show()

#plt.plot(tof)
##plt.yscale("log")
#plt.show()


#print(proplist)


#print(props)

##for p in props:
    ##print(list(p.keys()))
    ##print(p['intensity'])


#x_axis=[p["id"] for p in props]
#y_axis=[p["intensity"] for p in props]

#plt.plot(x_axis, y_axis, '*')
#plt.show()
