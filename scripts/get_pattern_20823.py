#!/usr/bin/env python3

import numpy as np
from sfdata import SFDataFiles
import argparse
from PIL import Image
import h5py
import glob
import os

def load_mask(filename):
    

    img = Image.open(filename).convert('L')
    mask=np.array(img, dtype=float)
    mask=mask/np.amax(mask)
    mask[mask<0.5]=0
    mask[mask>=0.5]=1
    
    return mask


def load_pattern_from_sf(filename, pids):
    """
    ....
    """

    with SFDataFiles(filename) as datafile:
        channel_name = "JF15T08V01"
        subset = datafile[[channel_name]]
        channel  = subset[channel_name]
        origpids = list(channel.datasets.pids[:,0])
        
        
        indexes = np.array([origpids.index(i) for i in pids])

        return channel[indexes]
        

parser = argparse.ArgumentParser()

parser.add_argument('run', help="runnumber", type=int)
parser.add_argument('acq', help="acqnumber", type=int)

parser.add_argument('pid', help="pid", type=int)
#parser.add_argument('mask', help="mask")



args = parser.parse_args()



pid = int(args.pid)
run = int(args.run)
acq = int(args.acq)

maskfilename = "/das/work/p20/p20823/analysis/online_analysis/olaf-conf/masks/mask.tiff"


foldername = "/sf/maloja/data/p20823/raw/run{:04d}*".format(run)

fullfoldername = glob.glob(foldername)

#print(fullfoldername)
#exit(0)

#filename = "/sf/maloja/data/p20823/raw/run{:04d}/data/acq{:04d}.*.h5".format(run, acq)
filename = os.path.join(fullfoldername[0], "data/acq{:04d}.*.h5".format(acq))

print(filename)

data = load_pattern_from_sf(filename, [pid])
mask = load_mask(maskfilename)

pattern=data[0]


outname="{:04d}_{:04d}_".format(run, acq)+str(pid)+".h5"

h5file = h5py.File(outname, "w")
h5file.create_dataset("data", data=pattern)
h5file.create_dataset("id", data=pid)
h5file.create_dataset("mask", data=mask)
h5file.create_dataset("run", data=run)
h5file.create_dataset("acq", data=acq)
h5file.create_dataset("filename", data=filename)


h5file.close()


import matplotlib.pyplot as plt
plt.imshow(np.log(pattern)*mask)
plt.show()
