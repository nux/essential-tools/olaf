'''
All the functions listed here are executed on the whole dataset once, and they have access to the whole dataset data.
The function must have the following form:

FUNCTION DEFINITION
-------------------

def FUNCION_NAME(settings, experiment_data, properties)

- settings: a dictionary containing all the settings loaded from the configuration file (yaml format) and parsed as nested dictionaries
- experiment_data: contains the data loaded from the h5 file, following the scheme indicated in the "h5" section of the configuration file.
- properties: previously computed properties, to allow conditional execution of the function basing for the previous analysis results

For example, if the diffraction patterns are in the dataset "vmi/andor" in the h5 file, and the configuration file contains the following:

h5:
    pattern: "vmi/andor"

then, the patterns are stored in experiment_data["pattern"].
All the data is stored such that the first index indicates the shot index within the dataset.
For example, the 2D pattern of the third shot in the dataset can be addressed here as experiment_data["pattern"][3]. it's previously computed property "markername" can be accessed via properties[3]["markername"]

FUNCTION RETURN
---------------

1) The function MUST return a dictionary or nothing. Each key of a dictionary defines a marker name in the analysis file. The dictionary can be empty, or no return value is given, which means that no marker is added to the analysis (useful, for example, if the function is used for data preparation)
2) The returned dictionary can contain multiple keys. The value of each key can be either:
    - a single value: in that case, the same property is replicated for each shot in the dataset
    - a list of values, where each item refers to a shot
    
    
IMPORTANT NOTES
---------------

* The functions are called in the order they are written in this file. 

* All the functions are executed. However, it is possible to create local functions, that are not directly called during the analysis, by giving them a name that starts with "_". For example, the following function:

def _get_sum(a,b):
    return a+b
    
is ok, and can be used by the other functions in this file. Instead, the function

def get_sum(a,b):
    return a+b
    
will be executed during the analysis.

* the types of the markers must be native python types, which means int, float and string

* These functions are not parallelized, as they act on the whole dataset. Be careful when a new computation is added, as it may hugely impact the execution time of the analysis.

'''

import numpy as np


