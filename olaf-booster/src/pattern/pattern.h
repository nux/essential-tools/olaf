#pragma once

#include <vector>
#include <array>
#include <tuple>
#include <memory>


class Pattern{
public:
    Pattern(double* data_in, std::array<std::size_t,2> dims_in, std::array<double,2> center_in, double dtheta_in):
            data(data_in), dims(dims_in), center(center_in), dtheta(dtheta_in){       
//             printf("%d,%d\n\n", dims[0], dims[1]);
//             printf("%f,%f\n\n", center[0], center[1]);

    };
    
    
    
    std::vector<double> get_radial_profile(std::vector<double> angles, double phi);
    std::vector<double> get_radial_profile(std::vector<double> angles, std::array<double,2>  phi_limits);
    std::vector<std::vector<double>> get_radial_profile_centers(std::vector<double> angles, std::array<double,2> phi_limits, std::vector<std::array<double,2>> centers);

    std::vector<double> get_angular_profile(std::vector<double> phi,  double angle);
    std::vector<double> get_angular_profile(std::vector<double> phi, std::array<double,2>  angle_limits);
    
    
    std::vector<std::vector<double>> get_polar_image(std::array<double,2> angle_limits, std::array<double,2> phi_limits,  int nangles, int nphi);

    
    double get_value(std::array<double,2> coords);
    double get_value_polar(std::array<double,2> coords);

    double get_data(std::array<std::size_t,2> coords);
    
    double* data;
    std::array<std::size_t,2>  dims;
    std::array<double,2> center;
    double dtheta;
    
    
};
