#include <vector>

#define _USE_MATH_DEFINES
#include <cmath>
#include <algorithm>
#include <stdio.h>
#include "pattern.h"
#include <random>

#define INNER_STEPS 10



void get_stddev(std::vector<double> data, double& average, double& stddev){
    
    double sum=0;
    double ssum=0;
    double count=0;
    
    for(auto val: data) if(val>=0){
        sum+=val;
        count+=1.;
    }
    
    if(count>2){
        average=sum/count;

        for(auto val: data) if(val>=0){
            ssum+=(val-average)*(val-average);
        
        }
        stddev=std::sqrt(ssum/(count-1));
    
    }
    else{
        average=-1;
        stddev=-1;
    } 
    
    
}




void get_absdev(std::vector<double> data, double& average, double& absdev){
    
    double sum=0;
    double ssum=0;
    double count=0;
    
    for(auto val: data) if(val>=0){
        sum+=val;
        count+=1.;
    }
    
    if(count>2){
        average=sum/count;

        for(auto val: data) if(val>=0){
            ssum+=std::abs(val-average);
        
        }
        absdev=ssum/(count-1);
    
    }
    else{
        average=-1;
        absdev=-1;
    } 
    
    
}


// // // // // // // // // // // // // // // // // // // // // // // // // 

double Pattern::get_data(std::array<std::size_t,2> coords){
    std::size_t index = coords[0] + coords[1] * this->dims[0];
    
//     printf("(%d,%d-->%d)\n", coords[0], coords[1], index);

    if(index>=this->dims[1] * this->dims[0]) return -1;
    else return this->data[index];        
    
}



double Pattern::get_value(std::array<double,2> coords){
    double outval=-1;

    if(coords[0]>=0 && coords[0]<double(this->dims[0]-1) && coords[1]>=0 && coords[1]<double(this->dims[1]-1) ){

        std::array<double,2> coords_rel = {coords[0]-std::floor(coords[0]), coords[1]-std::floor(coords[1])};
        std::array<std::size_t,2> index = {(std::size_t) std::floor(coords[0]), (std::size_t) std::floor(coords[1])};
//         printf("%f,%f   %d,%d\n",coords_rel[0], coords_rel[1], index[0], index[1]);
        double norm=0;
        double val=0;
        if(get_data({index[0], index[1]})>=0){
            double factor = (1.-coords_rel[0])*(1.-coords_rel[1]);
            val  +=  factor * get_data({index[0], index[1]});
            norm +=  factor;
        }
        
        if(get_data({index[0]+1, index[1]})>=0){
            double factor = (coords_rel[0])*(1.-coords_rel[1]);
            val  +=  factor * get_data({index[0]+1, index[1]});
            norm +=  factor;
        }
        
        if(get_data({index[0], index[1]+1})>=0){
            double factor = (1.-coords_rel[0])*(coords_rel[1]);
            val  +=  factor * get_data({index[0], index[1]+1});
            norm +=  factor;
        }
        
        if(get_data({index[0]+1, index[1]+1})>=0){
            double factor = (coords_rel[0])*(coords_rel[1]);
            val  +=  factor * get_data({index[0]+1, index[1]+1});
            norm +=  factor;
        }
            
        if(norm>0)  outval= val/norm;
        else outval=-1;
    }
    
//     printf("(%d,%d) %f,%f-->%f\n", this->dims[0], this->dims[1], coords[0],coords[1], outval);
    return outval;
    
}



double Pattern::get_value_polar(std::array<double,2> coords_polar){
    std::array<double,2>  coords= {coords_polar[0] * std::cos(coords_polar[1]/180.*M_PI)/this->dtheta + this->center[0],
                        coords_polar[0] * std::sin(coords_polar[1]/180.*M_PI)/this->dtheta + this->center[1]};

//     printf("(%d,%d),(%f,%f)  %f,%f-->%f,%f\n", this->dims[0], this->dims[1], this->center[0], this->center[1], coords_polar[0],coords_polar[1], coords[0], coords[1]);

    return get_value(coords);
    }



std::vector<double> Pattern::get_radial_profile(std::vector<double> angles, double phi){
    
    std::vector<double> profile(angles.size(), 0);
    std::vector<double> norm(angles.size(), 0);
    
    #pragma omp parallel for schedule(dynamic)
    for( std::size_t iang=0; iang<angles.size()-1; iang++){
            double dang=(angles[iang+1]-angles[iang])/double(INNER_STEPS);
//             exit(-1);
            std::size_t limit = INNER_STEPS;
            if(iang==angles.size()-1) limit++;
            
            for(std::size_t iin=0; iin<limit; iin++){

                double val=this->get_value_polar({angles[iang]+dang*double(iin), phi});

                if(val>0){
                    double factor=1.-dang*double(iin)/(angles[iang+1]-angles[iang]);
                
                    #pragma omp atomic
                    profile[iang]+=factor*val;
                    
                    #pragma omp atomic
                    norm[iang]+=factor;
                    
                    #pragma omp atomic
                    profile[iang+1]+=(1.-factor)*val;
                    
                    #pragma omp atomic
                    norm[iang+1]+=(1.-factor);
                    
                }
                
            }
    }
    
    
    for( std::size_t iang=0; iang<angles.size(); iang++){
            if(norm[iang]>0) profile[iang]/=norm[iang];
            else profile[iang]=-1;
        }
    
    return profile;  
    
}


std::vector<double> Pattern::get_radial_profile(std::vector<double> angles, std::array<double,2> phi_limits){
    
    std::vector<double> profile(angles.size(), 0);
    std::vector<double> norm(angles.size(), 0);
    
    
    #pragma omp parallel for schedule(dynamic) collapse(2)
    for(std::size_t y = 0; y<this->dims[1]; y++)
        for(std::size_t x = 0; x<this->dims[0]; x++){
            double xrel=(x-this->center[0]);
            double yrel=(y-this->center[1]);
            
            
            double angle=std::atan2(yrel, xrel)/M_PI*180.;
            while(angle+360. <= phi_limits[1]){ angle+=360.;}
            
            if(angle<=phi_limits[1] && angle>=phi_limits[0]){
                double radius = std::sqrt(xrel*xrel + yrel*yrel)*this->dtheta;
                int rad_index=-1;
//                 int ok=-1;
                
                for(int i=0; i<angles.size()-1; i++){
                    if(angles[i]<=radius && angles[i+1]>radius){
                        rad_index=i;
//                         printf("%d \n", rad_index);
                        break;                    
                    }
                }    
                
                
                if(rad_index>=0){
                    double val=this->get_data({x,y});
                    if(val>=0){
                        double factor=(angles[rad_index+1]-radius)/(angles[rad_index+1]-angles[rad_index]);
        
                        #pragma omp atomic
                        profile[rad_index] += val*factor;
                        
                        #pragma omp atomic
                        norm[rad_index] +=factor;
                        
                        #pragma omp atomic
                        profile[rad_index+1] += val*(1.-factor);
                        
                        #pragma omp atomic
                        norm[rad_index+1] +=(1.-factor);                    
                    }
                    
                    
                }

            }
            
            
        }
        
        
    
    for( std::size_t iang=0; iang<angles.size(); iang++){
            if(norm[iang]>0) profile[iang]/=norm[iang];
            else profile[iang]=-1;
        }
    
    return profile;  
    
}



std::vector<std::vector<double>> Pattern::get_polar_image(std::array<double,2> angle_limits, std::array<double,2> phi_limits,  int nangles, int nphi){
    
    std::vector<std::vector<double>> image(nangles, std::vector<double>(nphi,0));
    
    double dangle=(angle_limits[1]-angle_limits[0])/double(nangles);
    double dphi=(phi_limits[1]-phi_limits[0])/double(nphi);
    
//     int angle_sampling_steps=std::ceil(dangle/this->dtheta);
    int angle_sampling_steps=1;
    double dangle_sampling=dangle/double(angle_sampling_steps);
    
    #pragma omp parallel for schedule(dynamic)
    for(std::size_t iangle = 0; iangle<nangles; iangle++){
        double minangle = dangle*double(iangle)+dangle_sampling/2.+angle_limits[0];
        
        for(std::size_t iphi = 0; iphi<nphi; iphi++){
//             double dphi_sampling=dangle_sampling/this->dtheta;
            int phi_sampling_steps= int(std::ceil((minangle*dphi/180.*M_PI)/dangle_sampling));
//             int phi_sampling_steps= int(std::ceil(dphi/dphi_sampling));
            double dphi_sampling=dphi/double(phi_sampling_steps);
//             double maxangle = dangle*double(iangle+1)+dangle/2.;
            double minphi = dphi*double(iphi)+dphi_sampling/2.+phi_limits[0];
//             double maxphi = dphi*double(iphi+1)+dphi/2.; 
            
            
//             printf("angle=%f,dangle=%f, dangle_sampling=%f, dangle_nsampling=%d, minangle=%f --- phi=%f,dphi=%f, dphi_sampling=%f, dphi_nsampling=%d, minphi=%f\n", dangle*double(iangle) + dangle/2., dangle, dangle_sampling, angle_sampling_steps,minangle,
//                         dphi*double(iangle) + dphi/2., dphi, dphi_sampling, phi_sampling_steps,minphi);
            
            double val=0;
            double norm=0;

            for(int i=0;i<angle_sampling_steps; i++)
                for(int j=0;j<phi_sampling_steps; j++){

                    double val=get_value_polar({
                                minangle+(double(i)+0.5)*dangle_sampling,
                                minphi + (double(j)+0.5)*dphi_sampling});
                    if(val>=0){
                        image[iangle][iphi]+=val;
                        norm+=1.;
                    }
                    
                }
            if(norm>0) image[iangle][iphi]/=norm;
            else image[iangle][iphi]=-1.;
                
            
            
            
        }
    }
    
    return image;  
    
}


std::vector<double> Pattern::get_angular_profile(std::vector<double> phi, double angle){
    
    std::vector<double> profile(phi.size(), 0);
    std::vector<double> norm(phi.size(), 0);

    for( std::size_t iphi=0; iphi<phi.size()-1; iphi++){
            double dphi=(phi[iphi+1]-phi[iphi])/double(INNER_STEPS);
            int limit = INNER_STEPS;
            if(iphi==phi.size()-1) limit++;
        
            
            for(int iin=0; iin<limit; iin++){
                double val=this->get_value_polar({angle, phi[iphi]+dphi*double(iin)});
                if(val>0){
                    double factor=1.-dphi*double(iin)/(phi[iphi+1]-phi[iphi]);
                
                    profile[iphi]+=factor*val;
                    norm[iphi]+=factor;
                    
                    profile[iphi+1]+=(1.-factor)*val;
                    norm[iphi+1]+=(1.-factor);
                    
                }
            }
    }
    
    
    for( std::size_t iphi=0; iphi<phi.size(); iphi++){
            if(norm[iphi]>0) profile[iphi]/=norm[iphi];
            else profile[iphi]=-1;
        }
    
    
    return profile;  
    
}



std::vector<double> Pattern::get_angular_profile(std::vector<double> phi, std::array<double,2>  angle_limits){
    
    int nangles = std::ceil((angle_limits[1]-angle_limits[0])/this->dtheta*2);
    
    if(nangles<2) nangles=2;
    double dangle=(angle_limits[1]-angle_limits[0])/double(nangles-1);
    
    
    std::vector<double> profile(phi.size(), 0);
    std::vector<double> counts(phi.size(), 0);

    
    for( int iangle=0; iangle<nangles; iangle++){
        auto temp_profile= this->get_angular_profile(phi, angle_limits[0]+double(iangle)*dangle);
        
        for( std::size_t iphi=0; iphi<phi.size(); iphi++)
            if(temp_profile[iphi]>=0){
                profile[iphi]+=temp_profile[iphi];
                counts[iphi]+=1.;
            }
    }
    
    for( std::size_t iphi=0; iphi<phi.size(); iphi++){
        if(counts[iphi]>0){
            profile[iphi]/=counts[iphi];
        }
        else profile[iphi]=-1;
    }
    
    return profile;
}


