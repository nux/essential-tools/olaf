#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include <memory>
#include <vector>
#include <array>
#include <cmath>
#include <string>
#include <variant>
#include <tuple>

#include "pattern/pattern.h"
#include "mie/core_shell.h"
#include <stdio.h>

namespace py = pybind11;



PYBIND11_MODULE(olaf_booster, nu)
{ 


    
    py::module pm = nu.def_submodule("pattern");
    
       

    pm.def("get_radial_profile", [](py::array_t<double> data_in, double dtheta_in, std::array<double,2> center_in, std::vector<double> angles_in, std::variant<double,std::array<double,2> > phi_in){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, center_in, dtheta_in );
                               
                        auto* phi  = std::get_if<double>(&phi_in);                        
                        if(phi!=nullptr) return pattern.get_radial_profile(angles_in, *phi);
                        else      return pattern.get_radial_profile(angles_in, std::get<std::array<double,2>>(phi_in));

                        }, R"pbdoc(
                            Get the radial profile  for a given set of angles. 
                            
                            The function computes the radial profile at the given scattering angles in a given scattering direction :math:`\phi`. 
                            
                            
                            :param data: 2d numpy array, containing the diffraction image
                            :param dtheta: angular extension of a single pixel. Usually computed as the ratio between the maximum scattering angle and half the pixel width of the detector
                            :param center: 2-elements array containing the x and y coordinate of the pattern center
                            :param angles: array of scattering angles, that must be sorted by increasing values. 
                            :param phi: scattering direction at which the radial profile is computed. If a list of 2 values is given, they represent the minimum and maximum scattering direction at which the radial profile is computed.
                            
                            :returns: a list of values corresponding to the radial profile.
                            )pbdoc",py::return_value_policy::move,  py::arg("data"), py::arg("dtheta"),py::arg("center"), py::arg("angles"), py::arg("phi"));
    
    
    
    pm.def("get_angular_profile", [](py::array_t<double> data_in,  double dtheta_in, std::array<double,2> center_in, std::vector<double> phi_in, std::variant<double,std::array<double,2> >  angle_in){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, center_in, dtheta_in );
                                
                        auto* angle  = std::get_if<double>(&angle_in);                        
                        if(angle!=nullptr) return pattern.get_angular_profile(phi_in, *angle);
                        else      return pattern.get_angular_profile(phi_in, std::get<std::array<double,2>>(angle_in));
        
                        }, R"pbdoc(
                            Get the angular profile  for a given set of scattering directions. 
                            
                            The function computes the angular profile in the given scattering directions at a given scattering angle :math:`\theta`. 
                            
                            
                            :param data: 2d numpy array, containing the diffraction image
                            :param dtheta: angular extension of a single pixel. Usually computed as the ratio between the maximum scattering angle and half the pixel width of the detector
                            :param center: 2-elements array containing the x and y coordinate of the pattern center
                            :param phi: array of scattering directions, that must be sorted by increasing values. 
                            :param angle:  scattering direction  :math:`\theta` at which the angular profile is computed. If a list of 2 values is given, they represent the minimum and maximum scattering angles at which the angular profile is computed
                            
                            :returns: a list of values corresponding to the angular profile.
                        )pbdoc",py::return_value_policy::move,  py::arg("data"), py::arg("phi"), py::arg("dtheta"),py::arg("center"), py::arg("angle"));
    
     
    pm.def("get_polar_image", [](py::array_t<double> data_in, double dtheta_in, std::array<double,2> center_in, std::array<double,2> angles_in, std::array<double,2> phi_in, std::array<double,2> size){
                        Pattern pattern((double*) (std::size_t) data_in.request().ptr , std::array<std::size_t,2>{(std::size_t) data_in.request().shape[1], (std::size_t) data_in.request().shape[0]}, center_in, dtheta_in );
                               
                        return pattern.get_polar_image(angles_in, phi_in, size[0], size[1]);

                        }, R"pbdoc(
                            Get the image in polar coordinates, theta (scatterin angle) and phi. 
                            
                            
                            
                            :param data: 2d numpy array, containing the diffraction image
                            :param dtheta: angular extension of a single pixel. Usually computed as the ratio between the maximum scattering angle and half the pixel width of the detector
                            :param center: 2-elements array containing the x and y coordinate of the pattern center
                            :param angles: 2-elements array containing the minimum and maximum scattering angle at which the image is computed 
                            :param phi: 2-elements array containing the minimum and maximum scattering direction at which the image is computed 
                            :param size: 2-elements array containing the size of the output image
                            
                            :returns: image in polar coordinates
                            )pbdoc",py::return_value_policy::move,  py::arg("data"), py::arg("dtheta"),py::arg("center"), py::arg("angles"), py::arg("phi")=std::array<double,2>{0,360}, py::arg("size")=std::array<double,2>{200,200});
     

// // // // // // // // // // // // // // // // // //     
    
    py::module mm = nu.def_submodule("mie");
    
    
    mm.def("sphere", [](double radius, double delta_core, double beta_core, std::vector<double> angles){
            
                        return mie::core_shell(
                                2.*M_PI*radius, 1-delta_core, -beta_core,
                                2.*M_PI*radius, 1-delta_core, -beta_core,
                                angles);
        },
        R"pbdoc(
        Mie core shell simulation.
        
        This function simulates the radial profile of a spherical sample. The radial profile is computed at the given angles.
        
        :param radius: radius of the whole sample.
        :param delta: :math:`\delta` value of the sample
        :param beta: :math:`\beta` value of the sample           
        :param angles: scattering angles at which the radial scattering profile must be computed
        
        :returns: the scattering radial profile, resulting from the simulation with the given parameters.
           )pbdoc", py::arg("radius"),  py::arg("delta"), py::arg("beta"), py::arg("angles") );
    
        
}
